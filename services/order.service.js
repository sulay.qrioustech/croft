const mongoose = require("mongoose");
const Order = mongoose.model("Order");
const Cart = mongoose.model("Cart");
const User = mongoose.model("User");
const Product = mongoose.model("Product");
const Transaction = mongoose.model("Transaction");
const PaymentService = require("./payment.service");
const common_function = require("../common_function/index");
var moment = require("moment");
var Distance = require('geo-distance');

exports.placeorder = async req => {
  console.log("HERE")
  try {
    // let order = new Order();
    // order.user_id = req.user_id;
    // order.cart_id = req.cart_id;
    // order.soldby_id = req.soldby_id;
    // order.total = req.total;
    // return await order.save()
    let cart_id = req.cart_id;
    let cart_details = await Cart.findOne({ _id: req.cart_id });
    // console.log("cart=>", cart_details);
    //console.log('user id',list.user_id);
    if (req.order_type == "pickup") {
      console.log(req.order_type);
      var list = await Cart.findOneAndUpdate(
        { _id: cart_id },
        { status: 1, order_type: req.order_type, time_slot: req.time, date: req.date },
        { new: true, upsert: true }
      );
    } else {
      console.log("1 =>", parseFloat(cart_details.total));
      console.log("2 =>", parseFloat(req.delivery_charge));


      //delivery_lat,delivery_long
      // long : owner_details.location.coordinates[0]
      // lat : owner_details.location.coordinates[1]
      var farm_detail = await Cart.findOne({ _id  : cart_id}).populate("owner_id")
      var to = {
        lat: req.delivery_lat,
        lon: req.delivery_long
      };
      console.log("FARM_DETAIL",farm_detail);
      var from = {
        lat: farm_detail.owner_id.location.coordinates[1],
        lon: farm_detail.owner_id.location.coordinates[0]
      };
      console.log("FROM",from);
      console.log("TO",to);
      var distanceinkm = Distance.between(from, to);
      console.log('DISTANCE' , distanceinkm.human_readable());
      var distanceavailable = parseInt(farm_detail.owner_id.farm.delivery_range)
      console.log('DISTANCE' , distanceavailable);
      if(distanceinkm > Distance(distanceavailable + ' km')){
        return false;//"Delivery not available at this location"
      }

      const total =
        parseFloat(cart_details.total) + parseFloat(req.delivery_charge);
      console.log("2");
      var list = await Cart.findOneAndUpdate(
        { _id: cart_id },
        {
          status: 1,
          order_type: req.order_type,
          time_slot: req.time,
          address: req.address,
          delivery_charge: req.delivery_charge,
          delivery_lat: req.delivery_lat,
          delivery_long: req.delivery_long,
          date: req.date,
          total: total
        },
        { new: true, upsert: true }
      );
    }
    const user_details = await User.findOne({ _id: list.user_id });
    const owner_details = await User.findOne({ _id: list.owner_id });
    //notification code
    var payload = {
      notification: {
        title: "Order Request",
        body:
          user_details.fname +
          " " +
          user_details.lname +
          " has sent you an order request",
        noti_type: "1"
      }
    };

    common_function.send_notification(payload, owner_details.fcm_id);

    if (!list) console.error("error occured while fetching carts...");
    return list;
  } catch (e) {
    // Log Errors
    console.log(e);
    throw e;
  }
};

exports.getProductDetail = async req => {
  try {
    let cart_id = req.cart_id;
    var list = await Cart.findOne({ _id: cart_id }).populate({
      path: "products.product_id"
    });

    //console.log('user id',list.user_id);
    const user_details = await User.findOne({ _id: list.user_id });
    const owner_details = await User.findOne({ _id: list.owner_id });

    if (!list) console.error("error occured while fetching carts...");
    return list;
  } catch (e) {
    console.log(e);
    throw e;
  }
};

exports.acceptorder = async req => {
  try {
    let data = await Cart.findById(req._id);

    //payment calculation
    
    let total = Number(data.total); 
    total = total + (total * 0.075)
    let fee = (total * 15) / 100;
    let owner_amount = total - fee;
    fee = fee.toFixed(2) * 100;
    charge_amount = Number(total).toFixed(2) * 100;
    
    console.log('TOTAL ',typeof total,total);
    console.log('FEE ',typeof fee,fee);
    console.log('STRIPE TOTAL  ', typeof charge_amount,charge_amount);
    console.log('Owner AMOUNT  ', typeof owner_amount,owner_amount);
    // console.log('user id',list.user_id);
    const user_details = await User.findOne({ _id: data.user_id });
    const owner_details = await User.findOne({ _id: data.owner_id });

    const payment = PaymentService.createCharge(
      charge_amount,
      fee,
      user_details.stripe_customer_id,
      owner_details.stripe_bank_id,
      req._id
    );

    if (payment) {
      const expiry_date = (dateFrom = moment(
        Date.now() + 7 * 24 * 3600 * 1000
      ).format("YYYY-MM-DD"));
      console.log("expiry_date =>", expiry_date);
      let response = await Cart.findOneAndUpdate(
        { _id: req._id },
        { status: 2, expiry_date: expiry_date },
        { new: true, upsert: true }
      );

      //transaction history code
      let transaction = new Transaction();
      transaction.user_id = data.user_id;
      transaction.cart_id = req._id;
      transaction.owner_id = data.owner_id;
      transaction.total = data.total;
      transaction.owner_amount = owner_amount;
      transaction.fee = fee / 100;
      transaction.save();

      //notification code
      var payload = {
        notification: {
          title: "Order Accepted",
          body:
            owner_details.fname +
            " " +
            owner_details.lname +
            " has accepted your order request",
          noti_type: "1"
        }
      };

      common_function.send_notification(payload, user_details.fcm_id);

      return response;
    } else {
      return;
    }
  } catch (e) {
    // Log Errors
    console.log(e);
    throw e;
  }
};

exports.rejectorder = async req => {
  try {
    let data = await Cart.findOneAndUpdate(
      { _id: req._id },
      { status: 3, reason: req.reason },
      { new: true, upsert: true }
    );

    //console.log('orders',data);
    const user_details = await User.findOne({ _id: data.user_id });
    const owner_details = await User.findOne({ _id: data.owner_id });

    // console.log('user_details', user_details);
    // console.log('owner_details', owner_details);
    //notification code
    var payload = {
      notification: {
        title: "Order Decline",
        body:
          owner_details.fname +
          " " +
          owner_details.lname +
          " has decline your order request",
        noti_type: "1"
      }
    };

    common_function.send_notification(payload, user_details.fcm_id);

    return data;
  } catch (e) {
    // Log Errors
    console.log(e);
    throw e;
  }
};

exports.trackorder = async req => {
  console.log("TRACK")
  let data = await Cart.findOneAndUpdate({ _id: req._id }, { tracking: true }, { new: true, upsert: true });
  console.log("CART", data);
  const user_details = await User.findOne({ _id: data.user_id });
  console.log("fname", user_details)
  var payload = {
    notification: {
      title: "Order Tracking",
      body:
        user_details.fname +
        " " +
        user_details.lname +
        " has start delivery for your order",
      noti_type: "1"
    }
  };

  common_function.send_notification(payload, user_details.fcm_id);

  return 'sucsess';
}

exports.rescheduleorder = async req => {
  try {
    let data = await Cart.findOneAndUpdate(
      { _id: req._id },
      { status: 1, reason: req.reason, isReschedule: true },
      { new: true, upsert: true }
    );

    //console.log('orders',data);
    const user_details = await User.findOne({ _id: data.user_id });
    const owner_details = await User.findOne({ _id: data.owner_id });

    // console.log('user_details', user_details);
    // console.log('owner_details', owner_details);
    //notification code
    var payload = {
      notification: {
        title: "Order Rescheduled",
        body:
          owner_details.fname +
          " " +
          owner_details.lname +
          " has reschedule your order",
        noti_type: "1"
      }
    };

    common_function.send_notification(payload, user_details.fcm_id);

    return data;
  } catch (e) {
    // Log Errors
    console.log(e);
    throw e;
  }
};

exports.listorders = async (req, res) => {
  res.send("List Order APi");
};

exports.boughtorder = async req => {
  try {
    var list = await Cart.find({ user_id: req.user_id, status: { $ne: 0 } })
      .populate({ path: "products.product_id" })
      .populate("owner_id");
    list = list.reverse();
    if (!list) console.error("error occured while fetching orders...");
    return list;
  } catch (e) {
    console.log(e);
    throw e;
  }
};

exports.sellorder = async req => {
  try {
    var list = await Cart.find({ owner_id: req.user_id, status: { $ne: 0 } })
      .populate({ path: "products.product_id" })
      .populate("owner_id")
      .populate("user_id");
    list = list.reverse();
    if (!list) console.error("error occured while fetching orders...");
    return list;
  } catch (e) {
    console.log(e);
    throw e;
  }
};

exports.check_quantity = async req => {
  try {
    let count = 0;
    var list = await Cart.find({ _id: req.cart_id }).populate({
      path: "products.product_id"
    });
    console.log("list", list[0].products);
    //var temp = list.toJSON();
    await list[0].products.map((item, i) => {
      console.log("abc", item.product_id);
      //console.log("quantity", item.product_id);
      if (parseInt(item.quantity) > parseInt(item.product_id.quantity)) {
        count = 1;
      }
    });
    return count;
  } catch (e) {
    console.log(e);
    throw e;
  }
};

exports.reduce_quantity = async req => {
  try {
    let quantity = 0;
    var datacart = await Cart.find({ _id: req.cart_id }).populate({
      path: "products.product_id"
    });
    let list = datacart;
    await list[0].products.map(async (item, i) => {
      console.log("abc", item.quantity);
      console.log("quantity", item.product_id.quantity);
      quantity = parseInt(item.product_id.quantity) - parseInt(item.quantity);
      console.log("final", quantity);
      let re = await Product.findOneAndUpdate(
        { _id: item.product_id._id },
        { quantity: quantity },
        { new: true, upsert: true }
      );
      //console.log("Data",re)
    });
  } catch (e) {
    console.log(e);
    throw e;
  }
};

exports.confirmorder = async id => {
  try {
    var data = await Cart.findOneAndUpdate(
      { _id: id },
      { status: 4, tracking: false },
      { new: true, upsert: true }
    );
  } catch (e) {
    console.log(e);
    throw e;
  }
};
