
const mongoose = require('mongoose');
const Review = mongoose.model('Review');
const Cart = mongoose.model("Cart");


/**
 * GET Reviews
 */
exports._getReviews = async function () {
    try {
        var p = await Review.find({})
        if (!p) {
            if (!list) console.error("error occured while fetching Reviews...")
        } else {
            return p
        }
    } catch (e) {
        // Log Errors
        console.log(e)
        throw e;
    }
}

/**
 * Add Review
 */
exports._addReview = async function (res) {
    try {
        let review = new Review();
        review.from_id = res.user_id;
        review.given_id = res.given_id;
        review.text = res.text;
        review.rating = res.rating;
        review.save((err, result) => {
            if (!err) {
                return result;
            }
            else {
                return err
            }
        })
    } catch (e) {
        // Log Errors
        console.log(e)
        throw e;
    }
}

/**
 * Get Review By Review Id
 */
exports._getReviewByReviewId = async function (_id) {
    try {
        var list = await Review.find({ _id });
        if (!list) console.error("error occured while fetching Reviews...")
        return list;
    } catch (e) {
        // Log Errors
        console.log(e)
        throw e;
    }
}


/**
 * Get Reviews List By USer Id
 */
exports._getReviewsByUserId = async function (user_id) {
    try {
        var list = await Review.find({ given_id : user_id }).populate("from_id");
        if (!list) console.error("error occured while fetching Reviews...")
        return list;
    } catch (e) {
        // Log Errors
        console.log(e)
        throw e;
    }
}

/**
 * Get status weather usercan review this farm or not
 */
exports._canUserReview = async function (req) {
    try {
        var list = await Cart.find({ user_id : req.user_id, owner_id : req.farm_id, status : 4 })
        if (list.length == 0) return false
        return true;
    } catch (e) {
        // Log Errors
        console.log(e)
        throw e;
    }
} 