
const mongoose = require('mongoose');
const Chat = mongoose.model('chat');

/**
 * Add Chat
 */
exports.add_chat = async function (req) {
    console.log("add_chat",req)
    try {
        let chat = new Chat();
        chat.room_id = req.room_id;
        chat.message = req.message;
        chat.save((err, result) => {
            if (!err) {
                console.log("result",result)
                return result;
            }
            else {
                console.log("err",err)
                return err
            }
        })
    } catch (e) {
        // Log Errors
        console.log(e)
        throw e;
    }
}


/**
 * Remove Chat
 */
exports.remove_chat = async function (req) {
    try {
        var p = await Chat.deleteOne({ id: req.chat_id });
        return p;
    } catch (e) {
        // Log Errors
        console.log(e)
        throw e;
    }
}

/**
 * Get Chats
 */
exports.get_chats = async function (req) {
    try {
        var p = await Chat.find({ room_id: req.room_id });
        
        console.log(req.room_id)
        return p

    } catch (e) {
        // Log Errors
        console.log(e)
        throw e;
    }
}