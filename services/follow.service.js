
const mongoose = require('mongoose');
const Follow = mongoose.model('Follow');

/**
 * Add Follow
 */
exports.add_status = async function (req) {
    try {
        let follow = new Follow();
        follow.user_id = req.user_id;
        follow.farm_id = req.farm_id;
        follow.save((err, result) => {
            if (!err) {
                return result;
            }
            else {
                return err
            }
        })
    } catch (e) {
        // Log Errors
        console.log(e)
        throw e;
    }
}


/**
 * Remove Follow
 */
exports.remove_status = async function (req) {
    try {
        var p = await Follow.deleteOne({ user_id: req.user_id, farm_id : req.farm_id});
        return p;
    } catch (e) {
        // Log Errors
        console.log(e)
        throw e;
    }
}

/**
 * Get Following status
 */
exports.get_follow_status = async function (req) {
    try {
        var p = await Follow.find({ user_id: req.user_id, farm_id : req.farm_id});
        if(p.length != 0){
            return true;
        }else{
            return false;
        }

    } catch (e) {
        // Log Errors
        console.log(e)
        throw e;
    }
}