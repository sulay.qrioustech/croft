const mongoose = require("mongoose");
const User = mongoose.model("User");
const Cart = mongoose.model("Cart");
const Product = mongoose.model("Product");
const Notification = mongoose.model("Notification");
const About = mongoose.model("About");
const Faq = mongoose.model("Faq");
const Feedback = mongoose.model("Feedback");
let bcrypt = require("bcryptjs");
let moment = require("moment");
const stripe = require("./stripe.service");

exports.setStripeToken = async (stripe_customer_id, user_id) => {
  try {
    let data = {
      _id: user_id,
      stripe_customer_id: stripe_customer_id,
      isCardUpdated: true
    };
    let user = new User();
    user = data;
    let res = await User.findOneAndUpdate({ _id: user_id }, user, {
      new: true,
      upsert: true
    });
    return res;
  } catch (e) {
    // Log Errors
    console.log(e);
    throw e;
  }
};

exports.setStripeBankToken = async (stripe_bank_id, user_id) => {
  try {
    let data = {
      _id: user_id,
      stripe_bank_id: stripe_bank_id,
      isBankUpdated: true
    };
    let user = new User();
    user = data;
    let res = await User.findOneAndUpdate({ _id: user_id }, user, {
      new: true,
      upsert: true
    });
    return res;
  } catch (e) {
    // Log Errors
    console.log(e);
    throw e;
  }
};

exports.createCharge = async (
  charge,
  fee,
  customer_id,
  acoount_id,
  cart_id
) => {
  try {
    await stripe.charges
      .create({
        amount: charge,
        currency: "usd",
        customer: customer_id,
        application_fee_amount: fee,
        capture: false,
        transfer_data: {
          destination: acoount_id
        }
      })
      .then(async paumentIntent => {
        console.log("payment log", paumentIntent);
        if (paumentIntent.status == "succeeded") {
          let res = await Cart.findOneAndUpdate(
            { _id: cart_id },
            { capture_id: paumentIntent.id },
            {
              new: true,
              upsert: true
            }
          );
          return true;
        } else {
          return false;
        }
        // res.send(paumentIntent)
      }).catch(error => {
        console.log("Create Charge", error);
        return false;
      });

    // if (stripe_customer_id !== '') {
    //     let rez = await PaymentService.setStripeToken(stripe_customer_id, data.user_id);
    //     res.status(200).json({ status: 1, msg: "Added Payment Info", data: rez });
    // }
  } catch (e) {
    console.log("Create Charge Catch" , e);
    return e
  }
};

exports.capture_transaction = async capture_id => {
  try {
    const payment = await stripe.charges.capture(capture_id);
    if (payment.status == "succeeded") {
      return true;
    } else {
      return payment;
    }
  } catch (e) {
    console.log(e);
    return e;
    // return res.status(412).json({ status: 0, msg: e });
  }
};
