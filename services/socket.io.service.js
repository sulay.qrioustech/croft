const mongoose = require("mongoose");
const User = mongoose.model("User");
const common_function = require("../common_function/index");

module.exports = (server) => {
    const mongoose = require('mongoose');
    const Chat = mongoose.model('chat');
    var socket = require("socket.io")
    var io;
    //merge socket with express server
    io = socket(server)

    //handle incomming connections from clients
    io.sockets.on("connection", (socket) => {
        console.log("connection")
        //once a client has connected, we expect to get a ping from them saying what room they want to join
        socket.on("room", (room) => {
            console.log("room", room)
            //Whenever there is a socket connection, the socket is connected to the requested room
            socket.join(room)
            //Whenever a message is send, the message event is fired with the data received upon, stored in the database and then sent back

            // For Live location Traking
            socket.on("location", (data) => {
                io.to(room).emit("location", data)
                // var usersInRoomAdapter = io.sockets.adapter.rooms[room];
                // var usersInRoom = usersInRoomAdapter.length;
            })

            // For Chat
            socket.on("message", async (data) => {
                let chat = new Chat();
                chat.room_id = room;
                chat.message = data.text;
                chat.status = data.user._id;
                chat.save((err, result) => {
                    if (!err) {
                        console.log("result", result)
                        return result;
                    }
                    else {
                        console.log("err", err)
                        return err
                    }
                })

                io.to(room).emit("message", data);
                let user_id = room.split("-")[1];
                let farm_id = room.split("-")[2];
                let user_details = await User.findOne({ _id : user_id },{ "fcm_id":1,"fname":1,"lname":1, "_id":0 })
                let farm_details = await User.findOne({ _id : farm_id },{ "fcm_id":1,"fname":1,"lname":1, "_id":0 })
                console.log("User Details",user_details);
                console.log("Farm Details",farm_details);

                var payload = {
                    notification: {
                      title: data.user._id == 1 ? user_details.fname + " " + user_details.lname : farm_details.fname + " " + farm_details.lname ,
                      body: data.text,
                      noti_type: "2"
                    }
                  };
                console.log("PAYLOAD",payload)
                common_function.send_notification(payload, data.user._id == 1 ? user_details.fcm_id : farm_details.fcm_id);
            }); 

            //Whenever a user starts typing, this event is fired.
            socket.on("typing", function (data) {
                io.to(room).emit("typing", data);
            });

            //Whenever a user stops typing, this event is fired.
            socket.on("stop_typing", function (data) {
                io.to(room).emit("stop_typing", data);
            });

        })
    })
}

// Room ID => room-user_id-farm_id