const mongoose = require("mongoose");
const User = mongoose.model("User");
const Cart = mongoose.model("Cart");
const Product = mongoose.model("Product");
const Notification = mongoose.model("Notification");
const About = mongoose.model("About");
const Faq = mongoose.model("Faq");
const Feedback = mongoose.model("Feedback");
const Transaction = mongoose.model("Transaction");
let bcrypt = require("bcryptjs");
let moment = require("moment");
let ObjectId = require("mongodb").ObjectID;

/**
 * Update User Schema
 */
exports._updateUser = async function (res) {
  try {
    let user = new User();
    user = res;
    //console.log('update log',user);
    let data = await User.findOneAndUpdate({ _id: res._id }, user, {
      new: true,
      upsert: true,
    });
    return data;
  } catch (e) {
    // Log Errors
    console.log(e);
    throw e;
  }
};

/**
 * Get Users List
 */
exports._getUsers = async function () {
  try {
    var p = await User.find({});
    return p;
  } catch (e) {
    // Log Errors
    console.log(e);
    throw e;
  }
};

exports._getUsersForAdmin = async function (_id) {
  try {
    let rez;
    let response = await User.find(
      { _id: _id, userType: "admin" },
      async (err, result) => {
        if (result.length > 0) {
          // var p = await User.find({});
          // rez =  p;
          rez = "";
        } else {
          rez = { status: 0, msg: "Email-Id doesn't exists" };
        }
      }
    );

    if (rez === "") {
      var p = await User.find({ userType: "user" });
      return p;
    } else {
      return { status: 0, msg: "Email-Id doesn't exists" };
    }

    return !response ? rez : response;
  } catch (e) {
    // Log Errors
    console.log(e);
    throw e;
  }
};

/**
 * Get Users List By User Id
 */
exports._getUserByUserId = async function (_id) {
  try {
    var list = await User.find({ _id });
    if (!list) console.error("error occured while fetching User...");
    return list;
  } catch (e) {
    // Log Errors
    console.log(e);
    throw e;
  }
};

/**
 * Get Farm Detail By USer Id
 */
exports._getFarmByUserId = async function (user_id) {
  try {
    var list = await User.find({ _id: user_id });
    if (!list) console.error("error occured while fetching Farm...");
    return list[0].farm;
  } catch (e) {
    // Log Errors
    console.log(e);
    throw e;
  }
};

/**
 * Get Nearby Farms list
 */
exports._getNearbyFarms = async function (data) {
  try {
    var list;
    // var data = {
    //     longitude: 72.4354354,
    //     latitude: 23.56456
    // }
    list = await User.aggregate([
      {
        $geoNear: {
          near: {
            type: "Point",
            coordinates: [
              parseFloat(data.longitude),
              parseFloat(data.latitude),
            ],
          },
          distanceField: "distance",
          maxDistance: 100000,
          spherical: true,
        },
      },
      { $sort: { distance: -1 } }, // Sort nearest first
      {
        $lookup: {
          from: "products",
          localField: "_id",
          foreignField: "user_id",
          as: "product",
        },
      },
      {
        $match: {
          product: { $nin: [[]] },
          isFarmUpdated: true,
        },
      },
      {
        $lookup: {
          from: "reviews",
          localField: "_id",
          foreignField: "given_id",
          as: "review",
        },
      },
      {
        $lookup: {
          from: "follows",
          let: { id: "$_id", user_id: "$user_id" },
          pipeline: [
            { $match: { $expr: { $eq: ["$$id", "$farm_id"] } } },
            {
              $match: { $expr: { $eq: ["$user_id", ObjectId(data.user_id)] } },
            },
            { $count: "isfollowing" },
          ],
          as: "follow_id",
        },
      },
      {
        $unwind: {
          path: "$follow_id",
          includeArrayIndex: "0",
          preserveNullAndEmptyArrays: true,
        },
      },
      {
        $replaceRoot: {
          newRoot: { $mergeObjects: ["$$ROOT", "$follow_id"] },
        },
      },
      {
        $sort: {
          isfollowing: -1,
        },
      },
    ]);

    if (!list) console.error("error occured while getting Farms list");

    return list;
  } catch (e) {
    // Log Errors
    console.log(e);
    throw e;
  }
};

exports._getNearbyFarmsFilter = async function (data) {
  let matchObj = {};
  let deliverytypeObj = [];
  if (data.delivery_type.includes("pickup")) {
    deliverytypeObj.push({
      "farm.deliveryType": "pickup",
    });
  }

  if (data.delivery_type.includes("delivery")) {
    deliverytypeObj.push({
      "farm.deliveryType": "delivery",
    });
  }

  matchObj = {
    $and: [
      {
        isFarmUpdated: true,
      },
      {
        $or: deliverytypeObj,
      },
      {
        product: { $nin: [[]] },
      },
    ],
  };

  //{ "$sort": { "distance": -1 } },
  // let sort = {}
  // if(data.delivery_distance === "desc"){
  //     //desc
  //     sort = { "distance": 1 }
  // }else{
  //     //asc
  //     sort = { "distance": -1 }
  // }

  try {
    var list;
    list = await User.aggregate([
      {
        $geoNear: {
          near: {
            type: "Point",
            coordinates: [
              parseFloat(data.longitude),
              parseFloat(data.latitude),
            ],
          },
          distanceField: "distance",
          maxDistance: data.delivery_distance * 1609.34,
          spherical: true,
        },
      },
      // { "$sort": sort }, // Sort nearest first
      {
        $lookup: {
          from: "products",
          localField: "_id",
          foreignField: "user_id",
          as: "product",
        },
      },
      {
        $match: matchObj,
      },
      {
        $lookup: {
          from: "reviews",
          localField: "_id",
          foreignField: "given_id",
          as: "review",
        },
      },
    ]);

    if (!list) console.error("error occured while getting Farms list");

    return list;
  } catch (e) {
    // Log Errors
    console.log(e);
    throw e;
  }
};

exports._signin = async (data) => {
  try {
    let user = new User();
    user = data;
    let res;
    let response = await User.find(
      { email: data.email, userType: "admin" },
      (err, result) => {
        if (result.length > 0) {
          if (bcrypt.compareSync(data.password, result[0].password)) {
            res = { status: 1, data: result, msg: "Login Successfully" };
          } else {
            res = { status: 0, msg: "Password is incorrect please try again." };
          }
        } else {
          res = { status: 0, msg: "Email-Id doesn't exists" };
        }
      }
    );

    return !response ? response : res;
  } catch (e) {
    // Log Errors
    console.log(e);
    throw e;
  }
};

exports._signup = async (req) => {
  try {
    User.find({ email: req.body.email }, (err, result) => {
      //console.log(result);
      if (result.length > 0) {
        return { status: 0, msg: "Email id already exists" };
      } else {
        const salt = bcrypt.genSaltSync(10);
        const hash = bcrypt.hashSync(req.body.password, salt);
        let user = new User();
        user.fname = req.body.fname;
        user.lname = req.body.lname;
        user.email = req.body.email;
        user.password = hash;
        // user.phone = req.body.phone;
        // user.address = req.body.address;
        // user.city = req.body.city;
        // user.state = req.body.state;
        // user.zipcode = req.body.zipcode;
        user.location = {
          type: "Point",
          coordinates: [1, 2],
        };
        user.userType = "admin";
        user.save((err, result) => {
          if (!err) {
            //console.log('result ',result);
            return {
              status: 1,
              data: result,
              msg: "Registration done successfully.",
            };
          } else {
            console.log(err);
            return { err };
          }
        });
      }
    });
  } catch (e) {
    throw e;
  }
};

exports.add_address = async function (data) {
  try {
    console.log("id", data.body.user_id);
    let address_info = {
      address: data.body.address,
      city: data.body.city,
      state: data.body.state,
      zipcode: data.body.zipcode,
      lat: data.body.lat,
      long: data.body.long,
    };
    let result = await User.findOneAndUpdate(
      { _id: data.body.user_id },
      { $push: { address_list: address_info } },
      { safe: true, new: true, upsert: true }
    );
    return result;
  } catch (e) {
    console.log(e);
    throw e;
  }
};

exports._getFarms = async function () {
  try {
    list = await User.find({ isFarmUpdated: true });
    if (!list) console.error("error occured while getting Farms list");

    return list;
  } catch (e) {
    console.log(e);
    throw e;
  }
};

exports.order_list = async function () {
  try {
    list = await Cart.find({ status: 1 })
      .populate({ path: "products.product_id", select: "product_name" })
      .populate("owner_id", "fname lname")
      .populate("user_id", "fname lname");
    if (!list) console.error("error occured while getting Farms list");

    return list;
  } catch (e) {
    console.log(e);
    throw e;
  }
};

exports.product_details = async function (req) {
  try {
    var list = await Product.find({ _id: req.product_id }).populate("user_id");
    console.log(list);
    if (!list) console.error("error occured while getting Product list");

    return list;
  } catch (e) {
    console.log(e);
    throw e;
  }
};

exports.order_details = async function (req) {
  try {
    var list = await Cart.find({ _id: req.cart_id })
      .populate({ path: "products.product_id" })
      .populate("user_id", "fname lname")
      .populate("owner_id", "fname lname");
    if (!list) console.error("error occured while getting Product list");

    return list;
  } catch (e) {
    console.log(e);
    throw e;
  }
};

// exports.setStripeToken = async (stripe_customer_id,user_id) => {
//     try {
//         let data={
//             _id: user_id,
//             stripe_customer_id : stripe_customer_id,
//             isCardUpdated: true
//         };
//         let user = new User();
//         user = data;
//         let res = await User.findOneAndUpdate({ _id: user_id }, user, { new: true, upsert: true });
//         return res;
//     } catch (e) {
//         // Log Errors
//         console.log(e)
//         throw e;
//     }
// }

exports.logout = async (user_id) => {
  try {
    let res = await User.findOneAndUpdate(
      { _id: user_id },
      { fcm_id: "" },
      { new: true, upsert: true }
    );
    return res;
  } catch (e) {
    console.log(e);
    throw e;
  }
};

exports.graph = async (data) => {
  //const start_date = moment(data.start_date).format();
  //const end_date = moment(data.end_date).format();
  //console.log('start date', end_date);
  list = await User.aggregate([
    {
      $match: {
        createdAt: {
          // $gte: new Date("2020-02-01T06:57:42.321+00:00"),
          // $lte: new Date("2020-03-01T06:57:42.321+00:00")
          $gte: new Date(data.start_date),
          $lte: new Date(data.end_date),
        },
      },
    },
    {
      $group: {
        _id: "$createdAt",
        count: { $sum: 1 },
      },
    },
  ]);
  return list;
};

exports.getUserFcmIds = async (data) => {
  try {
    fcm_ids = [];
    for (var i = 0; i < data.user_id.length; i++) {
      let id = await User.find({ _id: data.user_id[i] });
      //console.log(id);
      fcm_ids.push(id[0].fcm_id);
    }
    return fcm_ids;
  } catch (e) {
    console.log(e);
    throw e;
  }
};

exports.addNotification = async (data) => {
  try {
    let notification = new Notification();
    notification.title = data.title;
    notification.description = data.description;
    notification.save((err, result) => {
      if (!err) {
        console.log("result ", result);
        return notification;
      } else {
        console.log("error", err);
        return { err };
      }
    });
  } catch (e) {
    console.log(e);
    throw e;
  }
};

exports.get_notification = async () => {
  try {
    let list = await Notification.find();
    return list;
  } catch (e) {
    console.log(e);
    throw e;
  }
};

exports.get_aboutus = async () => {
  try {
    let list = await About.find();
    return list;
  } catch (e) {
    console.log(e);
    throw e;
  }
};

exports.update_about = async (data) => {
  try {
    let list = await About.findOneAndUpdate(
      { _id: data._id },
      { description: data.description },
      { new: true, upsert: true }
    );
    return list;
  } catch (e) {
    console.log(e);
    throw e;
  }
};

exports.add_faq = async (data) => {
  try {
    let faq = new Faq();
    faq.question = data.question;
    faq.answer = data.answer;
    faq.save((err, result) => {
      if (!err) {
        console.log("result ", result);
        return result;
      } else {
        console.log("error", err);
        return { err };
      }
    });
  } catch (e) {
    console.log(e);
    throw e;
  }
};

exports.edit_faq = async (data) => {
  try {
    let res = await Faq.findOneAndUpdate(
      { _id: data._id },
      { question: data.question, answer: data.answer },
      { new: true, upsert: true }
    );
    return res;
  } catch (e) {
    console.log(e);
    throw e;
  }
};

exports.get_faq = async (data) => {
  try {
    let res = await Faq.findById(data._id);
    return res;
  } catch (e) {
    console.log(e);
    throw e;
  }
};

exports.faq = async (data) => {
  try {
    let res = await Faq.find();
    return res;
  } catch (e) {
    console.log(e);
    throw e;
  }
};

exports.delete_faq = async (data) => {
  try {
    let res = await Faq.findByIdAndRemove(data._id);
    return res;
  } catch (e) {
    console.log(e);
    throw e;
  }
};

exports.feedback = async () => {
  try {
    let res = await Feedback.find().populate("user_id", "fname lname");
    return res;
  } catch (e) {
    console.log(e);
    throw e;
  }
};

exports.transaction_history = async () => {
  try {
    let res = Transaction.find()
      .populate("user_id", "fname lname")
      .populate("owner_id", "fname lname");
    return res;
  } catch (e) {
    console.log(e);
    throw e;
  }
};

//lookup
// {
//     "from":"follows",
//     "let":{"id":"$_id", "user_id": "$user_id"},
//     "pipeline":[
//       {"$match":{"$expr":{"$eq":["$$id","$farm_id"]}}},
//       {"$match":{"$expr":{"$eq":["$user_id",ObjectId('5e2e94d8feccb21fc0610ef5') ]}}},
//       { "$count": "isfollowing" }],
//     "as":"follow_id"
//   }

//unwind
// {
//     path: '$follow_id',
//     includeArrayIndex: '0',
//     preserveNullAndEmptyArrays: true
//   }

//replaceroot
// {
//     newRoot: { $mergeObjects: [ "$$ROOT", "$follow_id" ] }
//   }

//sort
// {
//     'isfollowing': -1
//   }
