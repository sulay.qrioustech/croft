
const mongoose = require('mongoose');
const Feedback = mongoose.model('Feedback');

/**
 * Add Feedback
 */
exports._addFeedback = async function (res) {
    try {
        let feedback = new Feedback();
        feedback.user_id = res.user_id;
        feedback.text = res.text;
        feedback.description = res.description;
        feedback.save((err, result) => {
            if (!err) {
                return result;
            }
            else {
                return err
            }
        })
    } catch (e) {
        // Log Errors
        console.log(e)
        throw e;
    }
}

/**
 * Get Feedbacks List
 */
exports._getFeedbacks = async function () {
    try {
        var p = await Feedback.find({});

        return p;

    } catch (e) {
        // Log Errors
        console.log(e)
        throw e;
    }
}

/**
 * Get Feedbacks List By Id
 */
exports._getFeedbackByUserId = async function (user_id) {
    try {
        var list = await Feedback.find({ user_id });
        if (!list) console.error("error occured while fetching Feedbacks...")
        return list;
    } catch (e) {
        // Log Errors
        console.log(e)
        throw e;
    }
}