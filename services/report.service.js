
const mongoose = require('mongoose');
const Report = mongoose.model('Report');

exports._addReport = async function (res) {
    try {
        let report = new Report();
        report.user_id = res.user_id
        report.report_type = res.report_type
        report.report_to = res.report_to;
        report.message = res.message;
        if(res.report_to == 'farm'){
            report.farm_id = res.farm_id
        }else{
            report.product_id = res.product_id
        }
        return await report.save()
    } catch (e) {
        // Log Errors
        console.log(e)
        throw e;
    }
}