
const mongoose = require('mongoose');
const Product = mongoose.model('Product');

/**
 * Get Product
 */
exports._addProduct = async function (res) {
    try {
        let data;
        let product = new Product();
        product.user_id = res.user_id;
        product.product_name = res.product_name;
        product.seeded_date = res.seeded_date;
        product.harvesting_date = res.harvesting_date;
        product.organically_grown = res.organically_grown;
        product.order_type = res.order_type;
        product.price = res.price;
        product.description = res.description;
        product.image = res.image
        product.unit = res.unit;
        product.quantity = res.quantity;
        return await product.save()
    } catch (e) {
        // Log Errors
        console.log(e)
        throw e;
    }
}

/**
 * Update Product Schema
 */
exports._updateProducts = async function (res) {
    try {
        let product = new Product();
        product = res;
        let data = await Product.findOneAndUpdate({ _id: res._id, user_id : res.user_id }, product, { new: true, upsert: true });
        return data;
    } catch (e) {
        // Log Errors
        console.log(e)
        throw e;
    }
}

/**
 * Delete Product Schema
 */
exports._deleteProducts = async function (res) {
    try {
        let data = await Product.remove({ _id: res._id, user_id: res.user_id });
        return data;
    } catch (e) {
        // Log Errors
        console.log(e)
        throw e;
    }
}

/**
 * Get Products List
 */
exports._getProducts = async function () {
    try {
        var p = await Product.find({});

        return p;

    } catch (e) {
        // Log Errors
        console.log(e)
        throw e;
    }
}

/**
 * Get Products List By Product Id
 */
exports._getProductByProductId = async function (_id) {
    try {
        var list = await Product.find({ _id });
        if (!list) console.error("error occured while fetching Products...")
        return list;
    } catch (e) {
        // Log Errors
        console.log(e)
        throw e;
    }
}


/**
 * Get Products List By Product Id
 */
exports._getProductsByUserId = async function (user_id) {
    try {
        var list = await Product.find({ user_id });
        if (!list) console.error("error occured while fetching Products...")
        return list;
    } catch (e) {
        // Log Errors
        console.log(e)
        throw e;
    }
}


exports._getFilterProductsByUserId = async (user_id) => {
    try {
        var list = await Product.find({ user_id });
        if (!list) console.error("error occured while fetching Products...")
        return list;
    } catch (e) {
        // Log Errors
        console.log(e)
        throw e;
    }
}