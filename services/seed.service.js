
let GardenInfo = require('../models/gardeninfo.model')
var gardeninfoData = require('../constants/gardeninfo');

/**
 * Add first time data to database which is necessary
 */
exports.addDefaultSeeds = async function () {
  
    let data = await GardenInfo.find();
    if (!data.length > 0) {
        await this.addGardeninfo();
    }

}

/**
 * GardenInfo
 */
exports.addGardeninfo = async () => {
    GardenInfo.insertMany(gardeninfoData).then(() => { console.log("==> GardenInfo Added to Db") }).catch((err) => { console.log(err) })    
  
}