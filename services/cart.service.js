const mongoose = require("mongoose");
const Cart = mongoose.model("Cart");

/**
 * Add Cart
 */
exports.addtocart = async function(res) {
  try {
    let usercart = await Cart.find({ user_id: res.user_id, status: 0 });
    let isExist = usercart.length > 0 ? true : false;

    if (!isExist) {
      console.log("isExist", true);
      let cart = new Cart();
      cart.user_id = res.user_id;
      cart.products = res.products;
      cart.total = res.products[0].total;
      cart.owner_id = res.owner_id;
      cart.status = 0;
      return await cart.save();
    } else {
      let cart = new Cart();
      let isProductexist = false;
      let isQuantitySame = false;
      let index;
      let total = 0;
      await usercart[0].products.map((item, i) => {
        console.log("total log", item.total);
        //total = parseFloat(total) + parseFloat(item.total);
        console.log(item.product_id + " ", typeof res.products[0].product_id);
        if (item.product_id.toString() === res.products[0].product_id) {
          console.log("existttt", true);
          isProductexist = true;
          index = i;
          if (item.quantity === res.products[0].quantity) {
            isQuantitySame = true;
          }
        }
        return item;
      });

      if (isProductexist && isQuantitySame) {
        console.log("Productalrady exist", true);
        return "Product already exist";
      } else if (isProductexist && !isQuantitySame) {
        console.log("Productalrady exist quantitydiff", true);
        usercart[0].products[index].quantity = res.products[0].quantity;
        usercart[0].products[index].total = res.products[0].total;
        await res.products.map(async (item, i) => {
          console.log("total =>", item.total);
          total = (await parseFloat(total)) + parseFloat(item.total);
        });

        cart = usercart[0];
        cart.total = total;
        console.log();
        return await Cart.findOneAndUpdate({ _id: usercart[0]._id }, cart, {
          upsert: true
        });
      } else {
        console.log("Produc", true);
        usercart[0].products.push({
          product_id: res.products[0].product_id,
          quantity: res.products[0].quantity,
          total: res.products[0].total
        });

        await usercart[0].products.map(async (item, i) => {
          total = parseFloat(total) + parseFloat(item.total);
        });
        console.log("final total =>", total);
        cart = usercart[0];
        cart.total = total;
        return await Cart.findOneAndUpdate({ _id: usercart[0]._id }, cart, {
          upsert: true
        });
      }
    }
  } catch (e) {
    // Log Errors
    console.log(e);
    throw e;
  }
};

/**
 * Get CArt List By User Id
 */
exports.cartlist = async function(user_id) {
  try {
    var list = await Cart.findOne({ user_id: user_id, status: 0 })
      .populate({ path: "products.product_id" })
      .populate({ path: "owner_id" })
      .populate({ path: "user_id" });
    //if (!list) console.error("error occured while fetching carts...")
    return list;
  } catch (e) {
    // Log Errors
    console.log(e);
    throw e;
  }
};

exports.deleteCart = async res => {
  try {
    return await Cart.deleteOne({ _id: res.cart_id, user_id: res.user_id });
  } catch (e) {
    console.log(e);
  }
};

exports.deletecartItem = async res => {
  try {
    let usercart = await Cart.find({ user_id: res.user_id, status: 0 });

    let isExist = usercart.length > 0 ? true : false;

    if (!isExist) {
      return "Cart not exist";
    } else {
      let cart = new Cart();
      let isProductexist = false;
      let isQuantitySame = false;
      let index;
      await usercart[0].products.map((item, i) => {
        if (item.product_id.toString() === res.product_id) {
          isProductexist = true;
          index = i;
          // if (item.quantity === res.products[0].quantity) {
          //     isQuantitySame = true
          // }
        }
        return item;
      });

      // if (isProductexist && isQuantitySame) {
      //     //delete product from cart
      //     return "Product already exist"
      // } else if (isProductexist && !isQuantitySame) {
      //     //delete product from cart
      //     usercart[0].products[index].quantity = res.products[0].quantity;
      //     cart = usercart[0];
      //     return await Cart.findOneAndUpdate({ _id: usercart[0]._id }, cart, { upsert: true });
      // }
      if (isProductexist) {
        usercart[0].products = usercart[0].products.filter(
          (item, i) => i !== index
        );
        cart = usercart[0];
        let cart_details = await Cart.findOneAndUpdate(
          { _id: usercart[0]._id },
          cart,
          {
            upsert: true
          }
        );
        const data = await Cart.findById(cart_details._id);
        // console.log(data);
        // console.log("products =>", data.products);
        if (data.products.length > 0) {
          return data;
        } else {
          return await Cart.deleteOne({
            _id: data._id
          });
        }
      } else {
        return "Product not Exist in Cart";
      }
    }
  } catch (e) {
    // Log Errors
    console.log(e);
    throw e;
  }
};
