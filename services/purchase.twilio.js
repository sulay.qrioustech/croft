var client = require('twilio')('AC978baf777069126bc612456a2216f5af', '34ca603ed0667f5e93442614a1b2be9b');

var purchase = (areaCode) => {
  var phoneNumber;

  return client.availablePhoneNumbers('US').local.list({
        areaCode: areaCode,
        voiceEnabled: true,
        smsEnabled: true
  }).then(function(searchResults) {
    console.log(searchResults)
    if (searchResults.length === 0) {
      throw { message: 'No numbers found with that area code' };
    }

    return client.incomingPhoneNumbers.create({
      phoneNumber: searchResults[0].phoneNumber,
      voiceApplicationSid: 'AP594259193a4ae1e98ce5b04090ec16b8',
      smsApplicationSid: 'AP594259193a4ae1e98ce5b04090ec16b8'
    });
  }).then(function(number) {
    console.log("DYNAMIC NUMBER ", number);
    return number.sid;
  });
}

var release = async (sid) => {
  var res = await client.incomingPhoneNumbers(sid).remove();
  return res
}

module.exports = {
  purchase,
  release
}
//exports.purchase = purchase;