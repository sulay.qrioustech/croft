const nodemailer = require('nodemailer');
const multer = require('multer');
const notification = require('../services/notification.service');

exports.makeid = (length) => {
    //var length = 6;
    var result           = '';
    var characters       = 'ABCDEFGHJKMNOPQRSTUVWXYZabcdefghjkmnopqrstuvwxyz0123456789';
    var charactersLength = characters.length;
    for ( var i = 0; i < length; i++ ) {
       result += characters.charAt(Math.floor(Math.random() * charactersLength));
    }
    return result;
 };
 
 exports.sendMail = async(to,message,subject) => {
    let testAccount = await nodemailer.createTestAccount();

    // create reusable transporter object using the default SMTP transport
    let transporter = nodemailer.createTransport({
        host: "email-smtp.us-east-1.amazonaws.com",
        port: 465,
        secure: true, // true for 465, false for other ports
        auth: {
            user: 'AKIAVQILLLQJBYW33JEA', // generated ethereal user
            pass: 'BHZxHDAByZe7OQyj3f2kpwxhNAFkiaC2s2GP87Bh5PER' // generated ethereal password
        }
    });

    // send mail with defined transport object
    let info = await transporter.sendMail({
        from: 'Rype Farms <varun@rypefarms.com>', // sender address
        to: to, // list of receivers
        subject: subject, // Subject line
        //text: "Hello world?", // plain text body
        html: message // html body
    });

    console.log("Message sent: %s", info.messageId);
 };

 exports.send_notification = async(payload,fcm_id) =>{
     var options = {
         priority: "high",
         timeToLive: 60 * 60 * 24
     };

     var registrationTokens = [fcm_id];
     notification.messaging().sendToDevice(registrationTokens, payload, options)
         .then((response) => {
             //res.send({ msg: "Successfully sent message:", data: response })
             console.log("Successfully sent message:", response);
         })
         .catch((error) => {
             //res.send({ msg: "Error sending message:", data: error })
             console.log("Error sending message:", error);
         });

 };

exports.send_admin_notification = async (payload, fcm_id) => {
    var options = {
        priority: "high",
        timeToLive: 60 * 60 * 24
    };

    var registrationTokens = fcm_id;
    notification.messaging().sendToDevice(registrationTokens, payload, options)
        .then((response) => {
            //res.send({ msg: "Successfully sent message:", data: response })
            console.log("Successfully sent message:", response);
        })
        .catch((error) => {
            //res.send({ msg: "Error sending message:", data: error })
            console.log("Error sending message:", error);
        });

};


 