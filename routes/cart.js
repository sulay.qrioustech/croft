let express = require("express");
let router = express.Router();
const CartValidator = require("../validator/cart_middleware");
const CartController = require("../controllers/cart.controller");

router.post("/addtocart", CartValidator.addtocart, CartController.addtocart);

router.post("/cartlist", CartValidator.cartlist, CartController.cartlist);

router.post(
  "/deletecartItem",
  CartValidator.deletecartItem,
  CartController.deletecartItem
);

router.post("/deletecart", CartValidator.deletecart, CartController.deletecart);

module.exports = router;
