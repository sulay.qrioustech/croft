let express = require('express');
let router = express.Router();

const ChatValidator = require("../validator/chat_middleware");
const ChatController = require("../controllers/chat.controller");

router.post("/getchats",ChatValidator.getchats,ChatController.getchats);

module.exports = router;