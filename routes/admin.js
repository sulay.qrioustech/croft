let express = require('express');
let router = express.Router();
let bcrypt = require('bcryptjs');
const mongoose = require('mongoose');
const User = mongoose.model('User');

//load validator
const middleware = require('../validator/middleware');
const common_function = require('../common_function/index');
const UserService = require('../services/user.service');

//Controller
const FeedBackController = require('../controllers/feedback.controller')
const UserController = require('../controllers/user.controller')
const ReviewController = require('../controllers/review.controller') 
const ProductController = require('../controllers/product.controller') 

//Admin Panel=========================================================================

//router.post('/adminsignup', UserController.signup)

router.post('/adminsignin', middleware.signin, UserController.signin)
router.post('/userslist', middleware.isAdmin, UserController.userslist)
router.post('/userFarmdetail', middleware.user_detail, UserController.userDetail)
router.post('/farmslist', middleware.isAdmin, UserController.farmslist)
router.post('/orderlist',middleware.isAdmin,UserController.orderDetail)
router.post('/orderDetails',middleware.order_details,UserController.order_details)
router.post('/productdetail', middleware.product_details,UserController.productDetail)
router.post('/productlist', middleware.product_list, ProductController.filterlistByUserid)
router.post('/graph',UserController.graph);
router.post('/addNotification', middleware.send_notification,UserController.send_notification)
router.post('/getAllNotification', middleware.isAdmin, UserController.get_notification)
router.post('/getAboutData',middleware.isAdmin,UserController.get_aboutus)
router.post('/updateAbout',middleware.update_about,UserController.update_about);
router.get('/addabout',UserController.add_about);
router.post('/addfaq',middleware.add_faq,UserController.add_faq);
router.post('/editfaq',middleware.edit_faq,UserController.edit_faq);
router.post('/getfaq',middleware.get_faq,UserController.get_faq);
router.post('/faq',middleware.isAdmin,UserController.faq);
router.post('/deletefaq', middleware.get_faq,UserController.delete_faq);
router.post('/feedbacks',middleware.isAdmin,UserController.feedback);
router.post('/transaction-history',middleware.isAdmin,UserController.transaction_history);
// router.get('/userlist', UserController.list)


module.exports = router;