let express = require("express");
let router = express.Router();
let bcrypt = require("bcryptjs");
const mongoose = require("mongoose");
const User = mongoose.model("User");

//load validator
const middleware = require("../validator/middleware");
const common_function = require("../common_function/index");
const UserService = require("../services/user.service");

//Controller
const FeedBackController = require("../controllers/feedback.controller");
const UserController = require("../controllers/user.controller");
const ReviewController = require("../controllers/review.controller");

const PaymentController = require("../controllers/payment.controller");

const FollowController = require("../controllers/follow.controller");
const FollowValidator = require("../validator/follow_middleware");

//Login registeration modules

router.post("/signup", middleware.schemas, (req, res) => {
  User.find({ email: req.body.email }, (err, result) => {
    //console.log(result);
    if (result.length > 0) {
      res.json({ status: 0, msg: "Email id already exists" });
    } else {
      let email_verification_code = common_function.makeid(6);
      const salt = bcrypt.genSaltSync(10);
      const hash = bcrypt.hashSync(req.body.password, salt);
      //console.log(email_verification_code);
      let user = new User();
      user.fname = req.body.fname;
      user.lname = req.body.lname;
      user.email = req.body.email;
      user.password = hash;
      user.phone = req.body.phone;
      let da = {
        address: req.body.address,
        city: req.body.city,
        state: req.body.state,
        zipcode: req.body.zipcode,
        lat: req.body.lat,
        long: req.body.long,
      };
      user.address_list = [da];
      user.email_verification_code = email_verification_code;
      user.email_verification_status = 0;
      user.userType = "user";
      user.location = {
        type: "Point",
        coordinates: [1, 2],
      };
      user.fcm_id = req.body.fcm_id;
      user.save((err, result) => {
        if (!err) {
          //console.log('result ',result);
          var message =
            "Hello, Here is your email varification code <b>" +
            email_verification_code +
            "</b>";
          common_function.sendMail(
            req.body.email,
            message,
            "Email Verification"
          );
          // delete result.password
          res.json({
            status: 1,
            data: result,
            msg: "Registration done successfully.",
          });
        } else {
          console.log(err);
          res.send(err);
        }
      });
    }
  });
});

router.post("/email-verification", middleware.verify_email, (req, res) => {
  User.findOneAndUpdate(
    {
      _id: req.body._id,
      email_verification_code: req.body.email_verification_code,
    },
    { email_verification_status: 1 },
    { new: true },
    (err, result) => {
      if (!err) {
        res.json({ status: 1, msg: "Email Verification done successfully." });
      } else {
        res.json({
          status: 0,
          msg: "Invalid verification code please try again.",
        });
      }
    }
  );
});

router.post("/login", middleware.login, async (req, res) => {
  User.find(
    { email: { $regex: "^" + req.body.email + "$", $options: "i" } },
    async (err, result) => {
      if (result.length > 0) {
        //console.log(result);
        // console.log(result[0].password);
        if (bcrypt.compareSync(req.body.password, result[0].password)) {
          console.log("fcm_id ", req.body.fcm_id);
          await User.findOneAndUpdate(
            { _id: result[0]._id },
            { fcm_id: req.body.fcm_id },
            { safe: true, new: true, upsert: true }
          );
          if (result[0].email_verification_status === 0) {
            var message =
              "Hello, Here is your email varification code <b>" +
              result[0].email_verification_code +
              "</b>";
            common_function.sendMail(
              req.body.email,
              message,
              "Email Verification"
            );
          }
          res.json({ status: 1, data: result, msg: "Login Successfully" });
        } else {
          res.json({
            status: 0,
            msg: "Password is incorrect please try again.",
          });
        }
      } else {
        res.json({ status: 0, msg: "Email-Id doesn't exists" });
      }
    }
  );
});

router.post("/forgot_password", middleware.forgot_password, (req, res) => {
  let new_password = common_function.makeid(6);
  const salt = bcrypt.genSaltSync(10);
  const hash = bcrypt.hashSync(new_password, salt);

  User.findOneAndUpdate(
    { email: req.body.email },
    { password: hash },
    { new: true },
    (err, result) => {
      //console.log(result);
      if (result !== null) {
        let msg =
          "Hello, Here is your new password <b> " + new_password + "</b>";
        common_function.sendMail(req.body.email, msg, "Forgot Password");
        res.json({
          status: 1,
          msg: "Please check your email for new password",
        });
      } else {
        res.json({ status: 0, msg: "Email-Id doesn't exists." });
      }
    }
  );
});

router.post("/change-password", middleware.change_password, (req, res) => {
  User.findById(req.body._id, (err, result) => {
    console.log(err, result);
    if (result) {
      if (bcrypt.compareSync(req.body.old_password, result.password)) {
        const salt = bcrypt.genSaltSync(10);
        const hash = bcrypt.hashSync(req.body.new_password, salt);
        User.findOneAndUpdate(
          { _id: req.body._id },
          { password: hash },
          { new: true },
          (error, data) => {
            if (data !== null) {
              res.json({ status: 1, msg: "Password Change Successfully" });
            } else {
              res.json({ status: 0, msg: error });
            }
          }
        );
      } else {
        res.json({
          status: 0,
          msg: "Old Password is incorrect please try again.",
        });
      }
    } else {
      res.json({ status: 0, msg: "User doesn't exists" });
    }
  });
});

//Mobile apis ==============================================================

//Update User details
router.post("/update-user", UserController.updateUsers);

//Add Feedback
router.post(
  "/add-user-feedback",
  middleware.add_feedback,
  FeedBackController.addUserFeedback
);

// Get User detail by User Id
router.post("/detail", middleware.user_detail, UserController.userDetail);

//Farm detail
router.post(
  "/farm-detail",
  middleware.user_detail,
  UserController.userfarmdetail
);
router.post(
  "/farm-list",
  middleware.nearyby_farms,
  UserController._getNearbyFarms
);
router.post(
  "/farm-list-filter",
  middleware.nearyby_farms_filter,
  UserController._getNearbyFarmsFilter
);

//Review
router.post("/can-review", middleware.canreview, ReviewController.canreview);
router.post("/add-review", ReviewController.add);
router.post("/get-review", middleware.user_detail, ReviewController.list);

//Address
router.post("/add-address", middleware.add_address, UserController.add_address);

//payment
router.post("/save-token", middleware.token, PaymentController.savetoken);
//router.post('/save-bank-details', middleware.token, PaymentController.savebanktoken);
router.post(
  "/save-bank-details",
  middleware.create_bank_acoount,
  PaymentController.savebanktoken
);
router.post("/charge", PaymentController.createCharge);

//logout
router.post("/logout", middleware.logout, UserController.logout);

//follow
router.post(
  "/follow",
  FollowValidator.follow,
  FollowController.change_follow_status
);

router.post(
  "/isfollowing",
  FollowValidator.follow,
  FollowController.get_follow_status
);

//get card detail
router.post(
  "/get-card-detail",
  middleware.logout,
  PaymentController.card_detail
);

router.post(
  "/get-bank-detail",
  middleware.logout,
  PaymentController.bank_detail
);

router.post(
  "/verified-email",
  middleware.verified_email,
  UserController.verified_email
);

const admin = require("../services/notification.service");
router.get("/noti", async (req, res) => {
  var payload = {
    notification: {
      title: "This is a Notification",
      body: "This is the body of the notification message.",
    },
  };

  var options = {
    priority: "high",
    timeToLive: 60 * 60 * 24,
  };

  var registrationTokens = [
    "cOXI7phhcpo:APA91bGbS7USlTAsImjBcDeyAQqWDxQep3dJE9ecpn_ckHZZMEsCKQJNmC5JHneoK8fNH2kCCWkXOZQ8097oHJVLizeMrzjbATUHPQ5w5Y9qhWS39I9wH6p8ufdpqvqhVs74SLL7Ekak",
  ];
  await admin
    .messaging()
    .sendToDevice(registrationTokens, payload, options)
    .then((response) => {
      res.send({ msg: "Successfully sent message:", data: response });
      //console.log("Successfully sent message:", response);
    })
    .catch((error) => {
      res.send({ msg: "Error sending message:", data: error });
      //console.log("Error sending message:",   error);
    });
});

router.post("/demo_charge", PaymentController.demo_charge);
router.post("/demo_capture", PaymentController.demo_capture);
router.get("/token", PaymentController.create_token);
router.post("/save_card", PaymentController.save_card);
router.post("/add_new_card", PaymentController.add_new_card);
router.post("/retrive_customer_card", PaymentController.retrive_customer);
router.post("/change_default_card", PaymentController.change_default_card);
module.exports = router;
