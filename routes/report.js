
let express = require('express');
let router = express.Router();
const ReportController = require('../controllers/report.controller') 
const middleware = require("../validator/middleware");

router.post("/sent", middleware.report,ReportController.addreport);

module.exports = router;