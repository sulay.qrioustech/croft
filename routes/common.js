let express = require('express');
let router = express.Router();
const GardenInfoController = require('../controllers/gardeninfo.controller') 


router.get("/getgardeninfo", GardenInfoController.getGardenInfo);

router.get("/geturls", (req,res) => {
    res.status(200).send({ privacypolicy : "http://rypefarms.com" , termsconditions : "http://rypefarms.com" })
});

module.exports = router;