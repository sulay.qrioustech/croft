var express = require('express'),
    aws = require('aws-sdk'),
    bodyParser = require('body-parser'),
    multer = require('multer'),
    multerS3 = require('multer-s3'),
    path = require("path");


let router = express.Router();
router.use(bodyParser.json());

/**
 * For File Upload on AWS
 */

aws.config.update({
    secretAccessKey: 'HOAJ2W18G6SzAEUIw4uf4Tmk1AvkK++6a/8tgdBz',
    accessKeyId: 'AKIAVQILLLQJKO2N2YXU',
    region: 'us-west-1'
});

s3 = new aws.S3();

var upload = multer({
    limits: { fieldSize: 25 * 1024 * 1024 },
    storage: multerS3({
        s3: s3,
        bucket: 'croft-products',
        contentType: multerS3.AUTO_CONTENT_TYPE,
        key: function (req, file, cb) {
            console.log("reQ : ", "Hello" , req );
            console.log("FILE : " , "Hello FILE " , file);
            let filename = Date.now() + "_" + file.originalname.replace(" ", "-"); //Date.now() + "_" +   .replace(" ", "-" );
            cb(null, filename ); //use Date.now() for unique file keys
        }
    }),
    limits: { fileSize: 50 * 1024 * 1024 },
}); 

//use by upload form
router.post('', upload.array('image'), function (req, res, next) {
    //console.log(req)
    if(req.files.length > 0){
        let paths = req.files.map((f) => path.basename(f.location))
        res.send(paths);
    }else{
        res.send("We didnt get any image here");
    }
});


/**
 * For File Upload to node server
 */
// const multer = require('multer');

// var storage = multer.diskStorage({
//     destination: function (req, file, cb) {
//         cb(null, 'public/')
//     },
//     filename: function (req, file, cb) {
//         console.log(file)
//         let filename = file.originalname.substr(0, file.originalname.lastIndexOf('.'));
//         let extension = file.originalname.substr(file.originalname.lastIndexOf('.') + 1);
//         cb(null, filename + '-' + Date.now() + "." + extension)
//     }
// })

// var upload = multer({ storage: storage }).single('image')

// app.post('/api/upload', function (req, res) {
//     upload(req, res, function (err) {
//         if (err instanceof multer.MulterError) {
//             return res.status(500).json(err)
//         } else if (err) {
//             return res.status(500).json(err)
//         }
//         return res.status(200).send(req.file)
//     })
// });

module.exports = router