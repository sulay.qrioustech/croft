let express = require("express");
let router = express.Router();
const OrderValidator = require("../validator/order_middleware");
const OrderController = require("../controllers/order.controller");

router.post(
  "/placeorder",
  OrderValidator.placeorder,
  OrderController.placeorder
);

router.post(
  "/acceptorder",
  OrderValidator.acceptorder,
  OrderController.acceptorder
);

router.post(
  "/rejectorder",
  OrderValidator.rejectorder,
  OrderController.rejectorder
);


router.post(
  "/rescheduleorder",
  OrderValidator.rescheduleorder,
  OrderController.rescheduleorder
);

router.post(
  "/boughtorder",
  OrderValidator.boughtorder,
  OrderController.boughtorder
);

router.post("/sellorder", OrderValidator.sellorder, OrderController.sellorder);

router.post(
  "/confirmorder",
  OrderValidator.confirmorder,
  OrderController.confirmorder
);

router.post(
  "/trackOrder",
  OrderValidator.trackorder,
  OrderController.trackorder
);

router.post(
  "/getBookedDeliverySlots",
  OrderValidator.getBookedDeliverySlots,
  OrderController.getBookedDeliverySlots
);

module.exports = router;
