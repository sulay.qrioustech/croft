let express = require("express");
let router = express.Router();
const ProductValidator = require("../validator/product_middleware");
const ProductController = require("../controllers/product.controller");

router.post(
  "/add",
  ProductValidator.add_product_details,
  ProductController.add
);

router.post(
  "/filterlist",
  ProductValidator.get_list,
  ProductController.filterlistByUserid
);

router.post(
  "/listByUserid",
  ProductValidator.get_list,
  ProductController.listByUserid
);

router.post(
  "/detailByProductId",
  ProductValidator.get_product_details,
  ProductController.detailByProductId
);

router.post("/update", ProductController.update);

router.post(
  "/delete",
  ProductValidator.get_list_by_productid,
  ProductController.delete
);

// Admin Panel

router.post("/productslist", ProductController.list);

module.exports = router;
