const mongoose = require("mongoose");
let express = require("express");
let router = express.Router();
const User = mongoose.model("User");
const Twiliocall = mongoose.model("Twiliocall");
var client = require('twilio')('AC978baf777069126bc612456a2216f5af', '34ca603ed0667f5e93442614a1b2be9b');
var purchaser = require('../services/purchase.twilio');

/**
 * Create Service
 */
// router.get("/createService", async (req, res) => {
//     await client.proxy.services.create({ uniqueName: 'rype_farms' }).then(service => console.log(service.sid)).catch(error => console.log(error))
//     // sid : KS4dae41a598e79908f13b397c6f2e5dd5
//     res.send("creates service")
// });

/**
 * GET PHONE NUMBERS
 */
// router.get("/getPhonenumbers", async (req, res) => {
//     var phonenumber = await purchaser.purchase(424)
//     console.log("PHONENUMBERS => ", phonenumber)
//     res.send({ txt: "GET NEW Phonenumber", data: phonenumber })
// })

/**
 * Relese PHONE NUMBERS
 */
// router.get("/releasePhonenumbers", async (req, res) => {
//     var phonenumber = await purchaser.release('PNf66e8efbc3b9fccbd86e1aaf76cc7c2a')
//     console.log("Release PHONENUMBERS => ", phonenumber)
//     res.send({ txt: "release Phonenumber", data: phonenumber })
// })





/**
 * Added Phone Number
 */

// router.get("/addPhoneNumbertoService", async (req, res) => {
//     await client.proxy.services('KS4dae41a598e79908f13b397c6f2e5dd5')
//         .phoneNumbers
//        // .create({ sid : 'PNa5d99fed2e39a498754d80a8b4b7b400' })
//        .create({ sid : 'PNd5fc0a290b3f5ee24aefb62b72f7a2b3' })
//        .then(phone_number => console.log(phone_number.sid))
//        .catch(error => console.log(error));
//         // sid : PNa5d99fed2e39a498754d80a8b4b7b400
//         // sid : PNd5fc0a290b3f5ee24aefb62b72f7a2b3
//     res.send("added Phone Number to service")
// });

/**
 * Create Session
 */

// router.get("/createSession", async (req, res) => {
//     await client.proxy.services('KS4dae41a598e79908f13b397c6f2e5dd5')
//         .sessions
//         .create({ uniqueName: 'room-client-farm' })
//         .then(session => console.log(session.sid))
//         .catch(error => console.log(error));
//     //room-user-farm => sid : KC426b5152ff9d1c39407735a0a6deb76b
//     //room-client-farm => sid : KC4a09745ca842fcb8d9f27bd2857f2be0
//     res.send("creates session")
// });


/**
 * Add 1st participant in the session
 */

// router.get("/add1stParticipant", async (req, res) => {
//     await client.proxy.services('KS4dae41a598e79908f13b397c6f2e5dd5')
//         .sessions('KC4a09745ca842fcb8d9f27bd2857f2be0')
//         .participants
//         .create({ friendlyName: 'Alice', identifier: '+13106916848' })
//         .then(participant => console.log(participant.proxyIdentifier))
//         .catch(error => console.log(error));
//     res.send("added 1st participant in the session")
//     //room-user-farm => +12013736063
//     //room-client-farm => +12013736063
// });

/**
 * Add 2nd participant in the session
 */

// router.get("/add2ndParticipant", async (req, res) => {
//     await client.proxy.services('KS4dae41a598e79908f13b397c6f2e5dd5')
//         .sessions('KC4a09745ca842fcb8d9f27bd2857f2be0')
//         .participants
//         .create({ friendlyName: 'Bob', identifier: '+14243624204' })
//         .then(participant => console.log(participant.proxyIdentifier))
//         .catch(error => console.log(error));
//     //room-user-farm => +16198212966
//     //room-client-farm  => +16198212966
//     res.send("added 2nd participant in the session")
// });



// router.post('/twiliocall', async (req, res) => {

//     const user_details = await User.findOne({ _id: req.body.user_id });
//     const farm_details = await User.findOne({ _id: req.body.farm_id });

//     /**
//      * Step 1 : Create a service with a unique name for the application
//      * await client.proxy.services.create({ uniqueName : 'rype_farms' })
//         .then(service => console.log(service.sid))
//         .catch(error => console.log(error))
//      */

//     /**
//      * Fetch the service
//      *  await client.proxy.services('KS4dae41a598e79908f13b397c6f2e5dd5')
//             .fetch()
//             .then(service => console.log(service.uniqueName));
//      */

//     /**
//      * Step 2 : Create a session with a unique name for the service
//      */
//     let session_sid, error_code = 0;
//     await client.proxy.services('KSb70959b86e1a57b826f9fe50052444a9')
//         .sessions
//         .create({ uniqueName: 'room-' + user_details._id + '-' + farm_details._id, ttl: 1200 })
//         .then(session => {
//             console.log(session.sid)
//             session_sid = session.sid
//         })
//         .catch(async error => {
//             /*{ [Error: Session UniqueName must be unique.]
//                 status: 400,
//                 message: 'Session UniqueName must be unique.',
//                 code: 80603,
//                 moreInfo: 'https://www.twilio.com/docs/errors/80603',
//                 detail: undefined }*/

//             if (error.code == 80603) {
//                 error_code = 80603
//                 // session already created

//             }
//             console.log(error)
//         })


//     if (error_code == 80603) {
//         let sessionDetails = await Twiliocall.findOne({ room: 'room-' + user_details._id + '-' + farm_details._id })
//         console.log(sessionDetails)
//         if (req.body.role == 'farm') {
//             return res.status(200).json({ status: 1, data: sessionDetails.user_phone })
//         } else {
//             return res.status(200).json({ status: 1, data: sessionDetails.farm_phone })
//         }
//     }


//     let twiliocall = new Twiliocall();
//     twiliocall.room = 'room-' + user_details._id + '-' + farm_details._id;
//     twiliocall.sid = session_sid;
//     await twiliocall.save();

//     /**
//      * Step 3 : Purchase a phone number and add in service for both the user
//      */

//     var phonenumber_sid1 = await purchaser.purchase(user_details.phone.toString().substring(1, 4))
//     await client.proxy.services('KSb70959b86e1a57b826f9fe50052444a9')
//         .phoneNumbers
//         .create({ sid: phonenumber_sid1 })
//         .then(phone_number => console.log(phone_number.sid))
//         .catch(error => console.log(error));

//     var phonenumber_sid2 = await purchaser.purchase(farm_details.phone.toString().substring(1, 4))
//     await client.proxy.services('KSb70959b86e1a57b826f9fe50052444a9')
//         .phoneNumbers
//         .create({ sid: phonenumber_sid2 })
//         .then(phone_number => console.log(phone_number.sid))
//         .catch(error => console.log(error));

//     /**
//      * Step 4 : Add Phone numbers in the session
//      */
//     let user_proxy_phone, farm_proxy_phone;
//     await client.proxy.services('KSb70959b86e1a57b826f9fe50052444a9')
//         .sessions(session_sid)
//         .participants
//         .create({ friendlyName: user_details.fname, identifier: '+' + user_details.phone })
//         .then(participant => {
//             console.log(participant.proxyIdentifier)
//             user_proxy_phone = participant.proxyIdentifier
//         })
//         .catch(error => console.log(error));

//     await client.proxy.services('KSb70959b86e1a57b826f9fe50052444a9')
//         .sessions(session_sid)
//         .participants
//         .create({ friendlyName: farm_details.fname, identifier: '+' + farm_details.phone })
//         .then(participant => {
//             console.log(participant.proxyIdentifier)
//             farm_proxy_phone = participant.proxyIdentifier
//         })
//         .catch(error => console.log(error));

//     /**
//      * Step 5 : Delete the session after some time
//      */

//     let sessionDetails = await Twiliocall.findOne({ sid: session_sid })
//     await Twiliocall.findOneAndUpdate({ _id: sessionDetails._id }, { farm_phone: farm_proxy_phone, user_phone: user_proxy_phone }, { new: true, upsert: true })

//     if (req.body.role == 'farm') {
//         return res.status(200).json({ status: 1, data: user_proxy_phone })
//     } else {
//         return res.status(200).json({ status: 1, data: farm_proxy_phone })
//     }

// });


module.exports = router;