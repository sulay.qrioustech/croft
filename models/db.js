const mongoose = require('mongoose');
const SeedService = require("../services/seed.service");
require('./users.model');
require('./gardeninfo.model')
require('./feedback.model');
require('./product.model');
require('./review.model');
require('./cart.model');
require('./order.model');
require('./notification.model');
require('./about.model');
require('./faq.model');
require('./transaction.model');
require('./follow.model')
require('./chat.model')
require('./twiliomasking.model')
require('./report.model')
// mongoose.connect('mongodb://root:root@localhost:27017/croft?authSource=admin&readPreference=primary&appname=MongoDB%20Compass&ssl=false', {
mongoose.connect('mongodb://croftmongo:croft%4020%40%23@localhost:27017/croft?authSource=admin&readPreference=primary&appname=MongoDB%20Compass&ssl=falseType%20a%20message', {
//mongoose.connect('mongodb://croftmongo:croft%4020%40%23@18.144.93.143:27017/croft?authSource=admin&readPreference=primary&appname=MongoDB%20Compass&ssl=falseType%20a%20message', {
// mongoose.connect('mongodb://localhost:27017/croft', {
    useNewUrlParser: true
}, (err) => {
   if (!err) {
      console.log("Database Connected Successfully.")
      SeedService.addDefaultSeeds();
   } else {
      console.log('Error in DB Connection: ' + err)
   }
});
