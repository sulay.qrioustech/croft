const mongoose = require("mongoose");

let cartSchema = new mongoose.Schema(
  {
    user_id: {
      type: mongoose.Schema.ObjectId,
      ref: "User",
      required: true
    },
    products: [
      {
        product_id: {
          type: mongoose.Schema.ObjectId,
          ref: "Product"
        },
        quantity: String,
        total: String
      }
    ],
    status: {
      type: Number,
      default: 0
      //0- in cart
      //1- pending
      //2- accepted
      //3- rejected
      //4- completed
    },
    owner_id: {
      type: mongoose.Schema.ObjectId,
      ref: "User",
      required: true
    },
    total: {
      type: String
    },
    reason: {
      type: String
    },
    order_type: {
      type: String
    },
    time_slot: {
      type: String
    },
    address: {
      type: String
    },
    capture_id: {
      type: String
    },
    expiry_date: {
      type: String
    },
    delivery_charge: {
      type: String
    },
    isReschedule: {
      type : Boolean,
      default: false
    },
    tracking: {
      type : Boolean,
      default: false
    },
    delivery_lat:{
      type: String
    },
    delivery_long:{
      type: String
    },
    date:{
      type: String
    }
  },
  {
    timestamps: true
  }
);

module.exports = Content = mongoose.model("Cart", cartSchema);
