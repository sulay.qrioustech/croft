const mongoose = require('mongoose');

let followSchema = new mongoose.Schema({
    user_id: {
        type: mongoose.Schema.ObjectId,
        ref: 'User',
        required: true
    },
    farm_id: {
        type: mongoose.Schema.ObjectId,
        ref: 'User',
        required: true
    }
});

mongoose.model('Follow', followSchema);