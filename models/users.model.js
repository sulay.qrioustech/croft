const mongoose = require('mongoose');

let userSchema = new mongoose.Schema({
    fname:{
        type:String
    },
    lname:{
        type:String
    },
    email:{
        type:String
    },
    password:{
        type:String
    },
    phone:{
        type:Number
    },
    address_list : {
        type: Array
    },
    email_verification_code:{
        type:String
    },
    email_verification_status:{
        type:Number
    },
    location: {
        type: {
            type: String, // Don't do `{ location: { type: String } }`
            enum: ['Point'], // 'location.type' must be 'Point'
            default: 'Point'
        },
        coordinates: {
            type: [Number]
        }
    },
    farm : {
        name: {
            type: String
        },
        gardeninfo : {
            type: Object
        },
        days: {
            type: Array
        },
        time: {
            from: {
                type : String
            },
            to : {
                type : String
            }
        },
        delivery_range: {
            type: String
        },
        delivery_charge: {
            type: String
        },
        about_farm : {
            type : String
        },
        deliveryType : {
            type : Array
        },
        pickup : {
            address: {
                type: String
            },
            city: {
                type: String
            },
            state: {
                type: String
            },
            zipcode: {
                type: String
            },
            lat: {
                type: Number
            },
            long: {
                type: Number
            },
        }
    },
    feedback: {
        type: String
    },
    userType : {
        type : String,
        enum : [ "admin" , "user" ]
    },
    isFarmUpdated : {
        type : Boolean,
        default : false
    },
    isCardUpdated: {
        type: Boolean,
        default: false
    },
    isBankUpdated: {
        type: Boolean,
        default: false
    },
    stripe_customer_id:{
        type:String
    },
    stripe_bank_id : {
        type: String
    },
    fcm_id:{
        type:String
    }
}, {
    timestamps: true
});

userSchema.index({ location: "2dsphere" });


mongoose.model('User',userSchema);


// {
//     createdAt: {
//         $gte: ISODate("2020-02-01T06:57:42.321+00:00"),
//             $lte : ISODate("2020-03-01T06:57:42.321+00:00")
//     }
// }
//165

// {
//     createdAt: {
//         $gte: ISODate("2020-02-01T06:57:42.321+00:00"),
//             $lte : ISODate("2020-03-01T06:57:42.321+00:00")
//     }
// }