const mongoose = require('mongoose');

let orderSchema = new mongoose.Schema({
    user_id:{
        type: mongoose.Schema.ObjectId,
        ref: 'User',
        required: true
    },
    cart_id : {
        type: mongoose.Schema.ObjectId,
        ref: 'Cart',
        required: true 
    },
    soldby_id: {
        type: mongoose.Schema.ObjectId,
        ref: 'User',
        required: true
    },
    isAccepted: false,
    isRejected : false,
    total : {
        type : String
    },
    reason:{
        type:String
    },
    order_type: {
        type: String
    },
    time_slot: {
        type: String
    },
    address: {
        type: String
    }
}, {
    timestamps: true
});

module.exports = Content = mongoose.model("Order", orderSchema);