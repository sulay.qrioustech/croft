const mongoose = require('mongoose');

let chatSchema = new mongoose.Schema({
    room_id: {
        type: String,
        required: true
    },
    message: {
        type: String,
        required: true
    },
    status: {
        type: String,
        required: true
        // 1 => farm
        // 2=> user
    }
},{
    timestamps: true
});

mongoose.model('chat', chatSchema);