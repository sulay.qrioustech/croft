const mongoose = require('mongoose');

let transactionSchema = new mongoose.Schema({
    user_id: {
        type: mongoose.Schema.ObjectId,
        ref: 'User',
        required: true
    },
    cart_id: {
        type: mongoose.Schema.ObjectId,
        ref: 'Cart',
        required: true
    },
    owner_id: {
        type: mongoose.Schema.ObjectId,
        ref: 'User',
        required: true
    },
    total: {
        type: String
    },
    owner_amount: {
        type: String
    },
    fee: {
        type: String
    }
}, {
    timestamps: true
});

module.exports = Content = mongoose.model("Transaction", transactionSchema);