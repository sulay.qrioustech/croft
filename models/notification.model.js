const mongoose = require('mongoose');

let notificationSchema = new mongoose.Schema({
    title: {
        type: String
    },
    description: {
        type: String
    }
},{
    timestamps: true
});

mongoose.model('Notification', notificationSchema);