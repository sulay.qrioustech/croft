const mongoose = require('mongoose');

let twiliocallSchema = new mongoose.Schema({
    room: {
        type: String
    },
    farm_phone : {
        type: Number
    },
    user_phone: {
        type: Number
    },
    sid : {
        type: String
    }
});

module.exports = Content = mongoose.model("Twiliocall", twiliocallSchema);