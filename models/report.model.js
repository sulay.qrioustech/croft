const mongoose = require('mongoose');

let reportSchema = new mongoose.Schema({
    user_id: {
        type: mongoose.Schema.ObjectId,
        ref: 'User',
        required: true
    },
    report_type : {
        type: String
        // 1 => Bug,
        // 2 => Bug,
        // 3 => Improvement
    },
    report_to: {
        type: String
        //farm,product
    },
    message: {
        type: String
    },
    farm_id : {
        type: mongoose.Schema.ObjectId,
        ref: 'User'
    },
    product_id : {
        type: mongoose.Schema.ObjectId,
        ref: 'Product'
    }
});

module.exports = Content = mongoose.model("Report", reportSchema);