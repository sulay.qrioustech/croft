const mongoose = require('mongoose');

let gardenInfoSchema = new mongoose.Schema({
    category_Name: {
        type: String
    },
    sub_Category : {
        type: Array
    }
});

module.exports = Content = mongoose.model("Gardeninfo", gardenInfoSchema);