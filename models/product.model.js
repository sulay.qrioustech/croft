const mongoose = require('mongoose');

let productSchema = new mongoose.Schema({
    user_id:{
        type: mongoose.Schema.ObjectId,
        ref: 'User',
        required: true
    },
    product_name:{
        type:String
    },
    seeded_date:{
        type:Date
    },
    harvesting_date:{
        type:Date
    },
    organically_grown:{
        type:Boolean
    },
    order_type:{
        type:Array
    },
    price:{
        type:Number
    },
    description:{
        type:String
    },
    image:{
        type:Array
    },
    unit : {
        type : String
    },
    quantity : {
        type : String
    } 
});

module.exports = Content = mongoose.model("Product", productSchema);