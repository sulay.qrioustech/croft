const mongoose = require('mongoose');

let aboutSchema = new mongoose.Schema({
    description: {
        type: String
    },
});

mongoose.model('About', aboutSchema);