const mongoose = require('mongoose');

let faqSchema = new mongoose.Schema({
    question: {
        type: String
    },
    answer: {
        type: String
    }
});

mongoose.model('Faq', faqSchema);