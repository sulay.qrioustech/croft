const mongoose = require('mongoose');

let feedbackSchema = new mongoose.Schema({
    text: {
        type: String
    },
    description: {
        type: String
    },
    user_id: {
        type: String,
        ref: 'User',
        required: true
    }
});

module.exports = Content = mongoose.model("Feedback", feedbackSchema);