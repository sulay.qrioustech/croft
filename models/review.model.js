const mongoose = require('mongoose');

let reviewSchema = new mongoose.Schema({
    text: {
        type: String
    },
    rating : {
        type: Number
    },
    from_id: {
        type: mongoose.Schema.ObjectId,
        ref: 'User',
        required: true
    },
    given_id : {
        type: mongoose.Schema.ObjectId,
        ref: 'User',
        required: true
    }
});

module.exports = Content = mongoose.model("Review", reviewSchema);