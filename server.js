require("./models/db");

let express = require("express");
let app = express();
var port = process.env.PORT || 3000;
// app.listen(3000);
var server = require('http').Server(app);
let bodyParser = require('body-parser')
var cors = require('cors')
var socketio = require('./services/socket.io.service')
require('dotenv').config()

let users = require("./routes/user.js");
let product = require("./routes/product.js");
let common = require("./routes/common.js");
let upload = require("./routes/upload.js");
let admin = require("./routes/admin.js");
let cart = require("./routes/cart.js");
let order = require("./routes/order.js");
let chat = require("./routes/chat.js");
let twilio = require("./routes/twilio.js");
let report = require("./routes/report.js");

app.use(cors());
// app.use(bodyParser.json());
app.use(bodyParser.json({limit:'50mb'})); 
app.use(bodyParser.urlencoded({extended:true, limit:'50mb'}));
app.use((req,res,next) => {
  var now = new Date().toString();
  var log = `${now}: ${req.method} ${req.url}`
  next();
})

// const middleware = require('./validator/middleware');

app.get("/api", (req, res) => {
  res.send("Api Route of CROFt");
});
app.use("/api/user", users);
app.use("/api/product", product);
app.use("/api/admin", admin);
app.use("/api", common);
app.use("/api/upload", upload);
app.use("/api/cart", cart);
app.use("/api/order", order);
app.use("/api/chat", chat);
// app.use("/api/twilio", twilio);
app.use("/api/report", report);


server.listen(port, () => {
  socketio(server);
  console.log('Server listening at port %d', port);
});

