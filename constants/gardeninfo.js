module.exports = [{
    category_Name: "Growing Methods", sub_Category: [
        { id: 1, name: "Inground Gardening", selected: true },
        { id: 2, name: "Certified Organic", selected: true },
        { id: 3, name: "Non-Certified Organic", selected: false },
        { id: 4, name: "Square Foot", selected: false },
        { id: 5, name: "Permaculture", selected: true },
        { id: 6, name: "Home Compost", selected: false },
        { id: 7, name: "Companion Planting", selected: false },
        { id: 8, name: "Intensive Gardening", selected: true },
        { id: 9, name: "Integrated Pest Management(IPM)", selected: false }
    ]
}, {
    category_Name: "Soil ammendments", sub_Category: [
        { id: 1, name: "Inground Gardening", selected: true },
        { id: 2, name: "Certified Organic", selected: true },
        { id: 3, name: "Non-Certified Organic", selected: false },
        { id: 4, name: "Square Foot", selected: false },
        { id: 5, name: "Permaculture", selected: true },
        { id: 6, name: "Home Compost", selected: false },
        { id: 7, name: "Companion Planting", selected: false },
        { id: 8, name: "Intensive Gardening", selected: true },
        { id: 9, name: "Integrated Pest Management(IPM)", selected: false }
    ]
}, {
    category_Name: "Plant Spaces", sub_Category: [
        { id: 21, name: "Unframed Beds", selected: true },
        { id: 22, name: "Straw Bale Beds", selected: true },
        { id: 23, name: "Vertical Gardening", selected: true },
        { id: 24, name: "Raised Beds", selected: false },
        { id: 25, name: "Hydroponic", selected: false },
        { id: 26, name: "Aquaponic", selected: false },
        { id: 27, name: "Aeroponic", selected: false },
        { id: 28, name: "Coontainer", selected: false }
    ]
}
]

