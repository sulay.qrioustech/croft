const Joi = require('@hapi/joi');

const follow = (req, res, next) => {
    
    let schema = Joi.object().keys({
        'user_id': Joi.string().required(),
        'farm_id': Joi.string().required()
    })

    const { error, value } = Joi.validate(req.body, schema);
    if (error == null) {
        next();
    }
    else {
        res.json({ status: 0, msg: 'Please fill all the details' });
    }
};

module.exports = { follow };