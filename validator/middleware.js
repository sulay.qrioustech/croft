const Joi = require("@hapi/joi");

const schemas = (req, res, next) => {
  const schema = Joi.object().keys({
    fname: Joi.string().required(),
    lname: Joi.string().required(),
    email: Joi.string().required(),
    password: Joi.string().required(),
    phone: Joi.required(),
    address: Joi.string().required(),
    city: Joi.string().required(),
    state: Joi.string().required(),
    zipcode: Joi.required(),
    fcm_id: Joi.string().required()
  });

  const { error, value } = Joi.validate(req.body, schema);
  // console.log(error);
  // console.log(value);
  if (error == null) {
    next();
  } else {
    res.json({ status: 0, msg: "Please fill all the details" });
  }

  //console.log(error);

  // define all the other schemas below
};

const verify_email = (req, res, next) => {
  const schema = Joi.object().keys({
    _id: Joi.string().required(),
    email_verification_code: Joi.string().required()
  });

  const { error, value } = Joi.validate(req.body, schema);
  // console.log(error);
  // console.log(value);
  if (error == null) {
    next();
  } else {
    res.json({ status: 0, msg: "Please fill all the details" });
  }
};

const login = (req, res, next) => {
  const schema = Joi.object().keys({
    email: Joi.string().required(),
    password: Joi.string().required(),
    fcm_id: Joi.string().required()
  });

  const { error, value } = Joi.validate(req.body, schema);
  // console.log(error);
  // console.log(value);
  if (error == null) {
    next();
  } else {
    res.json({ status: 0, msg: "Please fill all the details" });
  }
};

const forgot_password = (req, res, next) => {
  const schema = Joi.object().keys({
    email: Joi.string().required()
  });

  const { error, value } = Joi.validate(req.body, schema);
  // console.log(error);
  // console.log(value);
  if (error == null) {
    next();
  } else {
    res.json({ status: 0, msg: "Please fill all the details" });
  }
};

const update_profile = (req, res, next) => {
  const schema = Joi.object().keys({
    _id: Joi.string().required(),
    fname: Joi.string().required(),
    lname: Joi.string().required(),
    phone: Joi.required(),
    address: Joi.string().required(),
    city: Joi.string().required(),
    state: Joi.string().required(),
    zipcode: Joi.required()
  });

  const { error, value } = Joi.validate(req.body, schema);
  // console.log(error);
  // console.log(value);
  if (error == null) {
    next();
  } else {
    res.json({ status: 0, msg: "Please fill all the details" });
  }
};

const change_password = (req, res, next) => {
  const schema = Joi.object().keys({
    _id: Joi.string().required(),
    old_password: Joi.string().required(),
    new_password: Joi.string().required()
  });

  const { error, value } = Joi.validate(req.body, schema);
  // console.log(error);
  // console.log(value);
  if (error == null) {
    next();
  } else {
    res.json({ status: 0, msg: "Please fill all the details" });
  }
};

const update_farm_details = (req, res, next) => {
  const schema = Joi.object().keys({
    user_id: Joi.string().required(),
    name: Joi.string().required(),
    growning_method: Joi.array().required(),
    soil_ammendments: Joi.array().required(),
    plant_spaces: Joi.array().required()
  });

  const { error, value } = Joi.validate(req.body, schema);
  // console.log(error);
  // console.log(value);
  if (error == null) {
    next();
  } else {
    res.json({ status: 0, msg: "Please fill all the details" });
  }

  //console.log(error);

  // define all the other schemas below
};

const update_farm_location_details = (req, res, next) => {
  const schema = Joi.object().keys({
    user_id: Joi.string().required(),
    address: Joi.string().required(),
    lat: Joi.number().required(),
    long: Joi.number().required(),
    days: Joi.array().required(),
    time: Joi.string().required(),
    delivery_range: Joi.number().required(),
    delivery_charge: Joi.number().required()
  });

  const { error, value } = Joi.validate(req.body, schema);
  // console.log(error);
  // console.log(value);
  if (error == null) {
    next();
  } else {
    res.json({ status: 0, msg: "Please fill all the details" });
  }

  //console.log(error);

  // define all the other schemas below
};

const user_detail = (req, res, next) => {
  const schema = Joi.object().keys({
    user_id: Joi.string().required()
  });

  const { error, value } = Joi.validate(req.body, schema);
  // console.log(error);
  // console.log(value);
  if (error == null) {
    next();
  } else {
    res.json({ status: 0, msg: "UserId is required" });
  }
};

const add_feedback = (req, res, next) => {
  const schema = Joi.object().keys({
    user_id: Joi.string().required(),
    text: Joi.string().required(),
    description: Joi.string().required()
  });

  const { error, value } = Joi.validate(req.body, schema);
  // console.log(error);
  // console.log(value);
  if (error == null) {
    next();
  } else {
    res.json({ status: 0, msg: "Please fill all details" });
  }
};

const nearyby_farms = (req, res, next) => {
  const schema = Joi.object().keys({
    latitude: Joi.number().required(),
    longitude: Joi.number().required(),
    user_id: Joi.string().required()
  });

  const { error, value } = Joi.validate(req.body, schema);
  if (error == null) {
    next();
  } else {
    res.json({ status: 0, msg: "Please fill all details" });
  }
};

const nearyby_farms_filter = (req, res, next) => {
  const schema = Joi.object().keys({
    latitude: Joi.number().required(),
    longitude: Joi.number().required(),
    delivery_type: Joi.string(),
    delivery_distance: Joi.string(),
    organically_grown: Joi.string()
  });

  const { error, value } = Joi.validate(req.body, schema);
  if (error == null) {
    next();
  } else {
    res.json({ status: 0, msg: "Please fill all details" });
  }
};

const add_address = (req, res, next) => {
  const schema = Joi.object().keys({
    user_id: Joi.string().required(),
    address: Joi.string().required(),
    city: Joi.string().required(),
    state: Joi.string().required(),
    zipcode: Joi.string().required(),
    lat : Joi.string().required(),
    long : Joi.string().required()
  });

  const { error, value } = Joi.validate(req.body, schema);
  if (error == null) {
    next();
  } else {
    res.json({ status: 0, msg: "Please fill all details" });
  }
};
//Admin panel

const signin = (req, res, next) => {
  const schema = Joi.object().keys({
    email: Joi.string().required(),
    password: Joi.string().required()
  });

  const { error, value } = Joi.validate(req.body, schema);
  if (error == null) {
    next();
  } else {
    res.json({ status: 0, msg: "Please fill all details" });
  }
};

const isAdmin = (req, res, next) => {
  const schema = Joi.object().keys({
    admin_id: Joi.string().required()
  });

  const { error, value } = Joi.validate(req.body, schema);
  if (error == null) {
    next();
  } else {
    res.json({ status: 0, msg: "admin_id Key is Required" });
  }
};

const product_details = (req, res, next) => {
  const schema = Joi.object().keys({
    admin_id: Joi.string().required(),
    product_id: Joi.string().required()
  });

  const { error, value } = Joi.validate(req.body, schema);
  if (error == null) {
    next();
  } else {
    res.json({ status: 0, msg: "admin_id Key is Required" });
  }
};

const product_list = (req, res, next) => {
  const schema = Joi.object().keys({
    admin_id: Joi.string().required(),
    user_id: Joi.string().required()
  });

  const { error, value } = Joi.validate(req.body, schema);
  if (error == null) {
    next();
  } else {
    res.json({ status: 0, msg: "admin_id Key is Required" });
  }
};

const order_details = (req, res, next) => {
  const schema = Joi.object().keys({
    admin_id: Joi.string().required(),
    cart_id: Joi.string().required()
  });

  const { error, value } = Joi.validate(req.body, schema);
  if (error == null) {
    next();
  } else {
    res.json({ status: 0, msg: "admin_id Key is Required" });
  }
};

const card_details = (req, res, next) => {
  const schema = Joi.object().keys({
    user_id: Joi.string().required(),
    country: Joi.string().required(),
    currency: Joi.string().required(),
    account_holder_name: Joi.string().required(),
    // 'routing_number': Joi.string().required(),
    account_number: Joi.string().required()
  });

  const { error, value } = Joi.validate(req.body, schema);
  if (error == null) {
    next();
  } else {
    res.json({ status: 0, msg: "Please fill all the details" });
  }
};

const token = (req, res, next) => {
  const schema = Joi.object().keys({
    user_id: Joi.string().required(),
    token: Joi.string().required(),
    email: Joi.string().required()
  });

  const { error, value } = Joi.validate(req.body, schema);
  if (error == null) {
    next();
  } else {
    res.json({ status: 0, msg: "Please fill all the details" });
  }
};

const logout = (req, res, next) => {
  const schema = Joi.object().keys({
    user_id: Joi.string().required()
  });

  const { error, value } = Joi.validate(req.body, schema);
  // console.log(error);
  // console.log(value);
  if (error == null) {
    next();
  } else {
    res.json({ status: 0, msg: "Please fill all the details" });
  }
};

const send_notification = (req, res, next) => {
  const schema = Joi.object().keys({
    user_id: Joi.required(),
    title: Joi.string().required(),
    description: Joi.string().required()
  });

  const { error, value } = Joi.validate(req.body, schema);
  // console.log(error);
  // console.log(value);
  if (error == null) {
    next();
  } else {
    res.json({ status: 0, msg: "Please fill all the details" });
  }
};

const update_about = (req, res, next) => {
  const schema = Joi.object().keys({
    admin_id: Joi.required(),
    _id: Joi.string().required(),
    description: Joi.string().required()
  });

  const { error, value } = Joi.validate(req.body, schema);
  // console.log(error);
  // console.log(value);
  if (error == null) {
    next();
  } else {
    res.json({ status: 0, msg: "Please fill all the details" });
  }
};

const add_faq = (req, res, next) => {
  const schema = Joi.object().keys({
    admin_id: Joi.required(),
    question: Joi.string().required(),
    answer: Joi.string().required()
  });

  const { error, value } = Joi.validate(req.body, schema);
  // console.log(error);
  // console.log(value);
  if (error == null) {
    next();
  } else {
    res.json({ status: 0, msg: "Please fill all the details" });
  }
};

const edit_faq = (req, res, next) => {
  const schema = Joi.object().keys({
    admin_id: Joi.required(),
    _id: Joi.string().required(),
    question: Joi.string().required(),
    answer: Joi.string().required()
  });

  const { error, value } = Joi.validate(req.body, schema);
  // console.log(error);
  // console.log(value);
  if (error == null) {
    next();
  } else {
    res.json({ status: 0, msg: "Please fill all the details" });
  }
};

const get_faq = (req, res, next) => {
  const schema = Joi.object().keys({
    admin_id: Joi.required(),
    _id: Joi.string().required()
  });

  const { error, value } = Joi.validate(req.body, schema);
  // console.log(error);
  // console.log(value);
  if (error == null) {
    next();
  } else {
    res.json({ status: 0, msg: "Please fill all the details" });
  }
};

const create_bank_acoount = (req, res, next) => {
  const schema = Joi.object().keys({
    token: Joi.required(),
    user_id: Joi.string().required(),
    ssn : Joi.string().required(),
    dob : Joi.string().required(),
  });

  const { error, value } = Joi.validate(req.body, schema);
  // console.log(error);
  // console.log(value);
  if (error == null) {
    next();
  } else {
    res.json({ status: 0, msg: "Please fill all the details" });
  }
};

const verified_email = (req, res, next) => {
  const schema = Joi.object().keys({
    email: Joi.required(),
    code: Joi.string().required()
  });

  const { error, value } = Joi.validate(req.body, schema);
  // console.log(error);
  // console.log(value);
  if (error == null) {
    next();
  } else {
    res.json({ status: 0, msg: "Please fill all the details" });
  }
};

const canreview = (req, res, next) => {
  const schema = Joi.object().keys({
    user_id: Joi.required().required(),
    farm_id: Joi.string().required()
  });

  const { error, value } = Joi.validate(req.body, schema);
  if (error == null) {
    next();
  } else {
    res.json({ status: 0, msg: "Please fill all the details" });
  }
};

const report = (req, res, next) => {
  
  let schema;
  if(req.body.report_to == 'farm'){
    schema = Joi.object().keys({
      report_to: Joi.string().required(),
      user_id: Joi.string().required(),
      report_type: Joi.string().required(),
      message: Joi.string().required(),
      farm_id: Joi.string().required()
    });
  }else {
    schema = Joi.object().keys({
      report_to: Joi.string().required(),
      user_id: Joi.string().required(),
      report_type: Joi.string().required(),
      message: Joi.string().required(),
      product_id: Joi.string().required()
    });
  }
  
  const { error, value } = Joi.validate(req.body, schema);
  if (error == null) {
    next();
  } else {
    res.json({ status: 0, msg: "Please fill all the details" });
  }
};

module.exports = {
  schemas,
  verify_email,
  login,
  forgot_password,
  update_profile,
  nearyby_farms,
  nearyby_farms_filter,
  signin,
  change_password,
  add_feedback,
  update_farm_location_details,
  update_farm_details,
  user_detail,
  isAdmin,
  add_address,
  card_details,
  product_details,
  product_list,
  order_details,
  token,
  logout,
  send_notification,
  update_about,
  add_faq,
  edit_faq,
  get_faq,
  create_bank_acoount,
  verified_email,
  canreview,
  report
};
