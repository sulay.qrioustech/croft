const Joi = require("@hapi/joi");

const placeorder = (req, res, next) => {
  let schema;
  if (req.body.order_type == "pickup") {
    schema = Joi.object().keys({
      user_id: Joi.string().required(),
      cart_id: Joi.string().required(),
      order_type: Joi.string().required(),
      time: Joi.string().required(),
      date: Joi.string().required()
    });
  } else {
    schema = Joi.object().keys({
      user_id: Joi.string().required(),
      cart_id: Joi.string().required(),
      order_type: Joi.string().required(),
      time: Joi.string().required(),
      address: Joi.string().required(),
      delivery_charge: Joi.string().required(),
      delivery_lat: Joi.string().required(),
      delivery_long: Joi.string().required(),
      date: Joi.string().required()
    });
  }

  const { error, value } = Joi.validate(req.body, schema);
  if (error == null) {
    next();
  } else {
    res.json({ status: 0, msg: "Please fill all the details" });
  }
};

const acceptorder = (req, res, next) => {
  const schema = Joi.object().keys({
    _id: Joi.string().required()
    //'cart_id': Joi.string().required(),
  });

  const { error, value } = Joi.validate(req.body, schema);
  if (error == null) {
    next();
  } else {
    res.json({ status: 0, msg: "Please fill all the details" });
  }
};

const rejectorder = (req, res, next) => {
  const schema = Joi.object().keys({
    _id: Joi.string().required(),
    reason: Joi.string().required()
  });

  const { error, value } = Joi.validate(req.body, schema);
  if (error == null) {
    next();
  } else {
    res.json({ status: 0, msg: "Please fill all the details" });
  }
};


const rescheduleorder = (req, res, next) => {
  const schema = Joi.object().keys({
    _id: Joi.string().required(),
    reason: Joi.string().required()
  });

  const { error, value } = Joi.validate(req.body, schema);
  if (error == null) {
    next();
  } else {
    res.json({ status: 0, msg: "Please fill all the details" });
  }
};

const trackorder = (req, res, next) => {
  const schema = Joi.object().keys({
    _id: Joi.string().required()
    //'cart_id': Joi.string().required(),
  });

  const { error, value } = Joi.validate(req.body, schema);
  if (error == null) {
    next();
  } else {
    res.json({ status: 0, msg: "Please fill all the details" });
  }
};



const listorders = (req, res, next) => {
  const schema = Joi.object().keys({
    user_id: Joi.string().required()
  });

  const { error, value } = Joi.validate(req.body, schema);
  if (error == null) {
    next();
  } else {
    res.json({ status: 0, msg: "Please fill all the details" });
  }
};

const boughtorder = (req, res, next) => {
  const schema = Joi.object().keys({
    user_id: Joi.string().required()
  });

  const { error, value } = Joi.validate(req.body, schema);
  if (error == null) {
    next();
  } else {
    res.json({ status: 0, msg: "Please fill all the details" });
  }
};

const sellorder = (req, res, next) => {
  const schema = Joi.object().keys({
    user_id: Joi.string().required()
  });

  const { error, value } = Joi.validate(req.body, schema);
  if (error == null) {
    next();
  } else {
    res.json({ status: 0, msg: "Please fill all the details" });
  }
};

const confirmorder = (req, res, next) => {
  const schema = Joi.object().keys({
    user_id: Joi.string().required(),
    cart_id: Joi.string().required()
  });

  const { error, value } = Joi.validate(req.body, schema);
  if (error == null) {
    next();
  } else {
    res.json({ status: 0, msg: "Please fill all the details" });
  }
};

const getBookedDeliverySlots = (req, res, next) => {
  const schema = Joi.object().keys({
    farm_id: Joi.string().required(),
    date: Joi.string().required()
  });

  const { error, value } = Joi.validate(req.body, schema);
  if (error == null) {
    next();
  } else {
    res.json({ status: 0, msg: "Please fill all the details" });
  }
}; 

module.exports = {
  placeorder,
  listorders,
  rejectorder,
  acceptorder,
  boughtorder,
  sellorder,
  confirmorder,
  rescheduleorder,
  trackorder,
  getBookedDeliverySlots
};
