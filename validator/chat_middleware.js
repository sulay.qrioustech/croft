const Joi = require('@hapi/joi');

const getchats = (req, res, next) => {

    const schema = Joi.object().keys({
        'room_id': Joi.string().required()
    });

    const { error, value } = Joi.validate(req.body, schema);
    if (error == null) {
        next();
    }
    else {
        res.json({ status: 0, msg: 'Please fill all the details' });
    }
};


module.exports = { getchats };