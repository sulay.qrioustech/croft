const Joi = require("@hapi/joi");

const add_product_details = (req, res, next) => {
  const schema = Joi.object().keys({
    user_id: Joi.string().required(),
    product_name: Joi.string().required(),
    seeded_date: Joi.date().required(),
    harvesting_date: Joi.date().required(),
    organically_grown: Joi.boolean().required(),
    order_type: Joi.array().required(),
    price: Joi.number().required(),
    description: Joi.string().required(),
    image: Joi.array().required(),
    unit: Joi.string().required(),
    quantity: Joi.string().required()
  });

  const { error, value } = Joi.validate(req.body, schema);
  console.log("error", error);
  // console.log(value);
  if (error == null) {
    next();
  } else {
    res.json({ status: 0, msg: "Please fill all the details" });
  }

  //console.log(error);

  // define all the other schemas below
};

const get_list = (req, res, next) => {
  const schema = Joi.object().keys({
    user_id: Joi.string().required()
  });

  const { error, value } = Joi.validate(req.body, schema);
  // console.log(error);
  // console.log(value);
  if (error == null) {
    next();
  } else {
    res.json({ status: 0, msg: "user_id key is required" });
  }
};

const get_list_by_productid = (req, res, next) => {
  const schema = Joi.object().keys({
    user_id: Joi.string().required(),
    product_id: Joi.string().required()
  });

  const { error, value } = Joi.validate(req.body, schema);
  // console.log(error);
  // console.log(value);
  if (error == null) {
    next();
  } else {
    res.json({ status: 0, msg: "Please fill all Details" });
  }
};

const get_product_details = (req, res, next) => {
  const schema = Joi.object().keys({
    product_id: Joi.string().required()
  });

  const { error, value } = Joi.validate(req.body, schema);
  // console.log(error);
  // console.log(value);
  if (error == null) {
    next();
  } else {
    res.json({ status: 0, msg: "Please fill all Details" });
  }
};

module.exports = {
  add_product_details,
  get_list,
  get_list_by_productid,
  get_product_details
};
