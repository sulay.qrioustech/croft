const Joi = require('@hapi/joi');

const addtocart = (req, res, next) => {
    
    let product = Joi.object().keys({
        product_id: Joi.string().required(),
        quantity: Joi.string().required(),
        total: Joi.string().required()
    })

    const schema = Joi.object().keys({
        'owner_id': Joi.string().required(),
        'user_id': Joi.string().required(),
        'products': Joi.array().items(product).required()
    });

    const { error, value } = Joi.validate(req.body, schema);
    if (error == null) {
        next();
    }
    else {
        res.json({ status: 0, msg: 'Please fill all the details' });
    }
};

const cartlist = (req, res, next) => {

    const schema = Joi.object().keys({
        'user_id': Joi.string().required(),
    });

    const { error, value } = Joi.validate(req.body, schema);
    if (error == null) {
        next();
    }
    else {
        res.json({ status: 0, msg: 'Please fill all the details' });
    }
};

const deletecart = (req, res, next) => {

    const schema = Joi.object().keys({
        'user_id': Joi.string().required(),
        'cart_id': Joi.string().required(),
    });

    const { error, value } = Joi.validate(req.body, schema);
    if (error == null) {
        next();
    }
    else {
        res.json({ status: 0, msg: 'Please fill all the details' });
    }
};

const deletecartItem = (req, res, next) => {

    const schema = Joi.object().keys({
        'user_id': Joi.string().required(),
        'cart_id': Joi.string().required(),
        'product_id': Joi.string().required(),
    });

    const { error, value } = Joi.validate(req.body, schema);
    if (error == null) {
        next();
    }
    else {
        res.json({ status: 0, msg: 'Please fill all the details' });
    }
};

module.exports = { addtocart, cartlist, deletecart, deletecartItem };