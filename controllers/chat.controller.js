const ChatService = require('../services/chat.service')

exports.getchats = async (req, res) => {
    try {
        console.log(req.body)
        let rez = await ChatService.get_chats(req.body)
        if(rez.length > 0){
            rez = rez.map(item => {
                return {
                    _id : item._id,
                    text: item.message,
                    user: { _id: item.status },
                    createdAt : item.createdAt
                }
            })
        }
        console.log("CHat",rez);
        return res.status(200).json({ status: 1, msg: "Chat List", data: rez });
    } catch (e) {
        return res.status(412).json({ status: 0, msg: e });
    }
}