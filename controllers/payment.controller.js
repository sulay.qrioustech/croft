const UserService = require("../services/user.service");
const PaymentService = require("../services/payment.service");

const ReviewService = require("../services/review.service");
const stripe = require("../services/stripe.service");
const common_function = require("../common_function/index");
const mongoose = require("mongoose");
const About = mongoose.model("About");
const User = mongoose.model("User");

exports.savetoken = async (req, res) => {
  try {
    let data = req.body;
    let stripe_customer_id = "";

    await stripe.customers
      .create({
        name: data.token,
        email: data.email,
        source: data.token,
      })
      .then((customer) => {
        stripe_customer_id = customer.id;
      })
      .catch((err) => {
        console.log("create customer error ", err.raw.message);
        return res.status(200).json({
          status: 0,
          msg: err.raw.message + " Please check card details.",
        });
      });

    if (stripe_customer_id !== "") {
      let rez = await PaymentService.setStripeToken(
        stripe_customer_id,
        data.user_id
      );
      res.status(200).json({ status: 1, msg: "Added Payment Info", data: rez });
    }
  } catch (e) {
    console.log(e);
    return res.status(412).json({ status: 0, msg: e });
  }
};

exports.savebanktoken = async (req, res) => {
  try {
    let data = req.body;
    let bank_token = data.token;
    let stripe_bank_id = "";
    let dob = data.dob;
    let ssn = data.ssn;
    console.log("Bank Token", data.token);
    const user_details = await UserService._getUserByUserId(data.user_id);
    let account_holder_name,
      fname = "",
      lname = "";

    await stripe.tokens.retrieve(bank_token, async (err, token) => {
      console.log(token, err);
      if (err != null) {
        return res
          .status(200)
          .json({ status: 0, msg: "Bank Account Detail", data: err });
      }
      account_holder_name = token.bank_account.account_holder_name;
      if (account_holder_name.split(" ").length > 1) {
        fname = account_holder_name.split(" ")[0];
        lname = account_holder_name.split(" ")[1];
      } else {
        fname = account_holder_name.split(" ")[0];
      }
      console.log(account_holder_name);
      await stripe.accounts
        .create({
          type: "custom",
          country: "US",
          email: user_details[0].email,
          requested_capabilities: ["card_payments", "transfers"],
          external_account: bank_token,
          business_profile: {
            mcc: 5045,
            url: "https://cookies.com",
            name: user_details[0].farm.name,
            product_description: user_details[0].farm.about_farm,
          },
          business_type: "individual",
          tos_acceptance: {
            date: 1539356786,
            ip: "127.0.0.1",
          },
          individual: {
            id_number: user_details._id,
            first_name: fname,
            last_name: lname,
            address: {
              city: user_details[0].address_list[0].city,
              line1: user_details[0].address_list[0].address,
              postal_code: user_details[0].address_list[0].zipcode,
              state: user_details[0].address_list[0].state,
            },
            dob: {
              day: dob.split("-")[1],
              month: dob.split("-")[0],
              year: dob.split("-")[2],
            },
            phone: user_details[0].phone,
            email: user_details[0].email,
            ssn_last_4: ssn,
          },
        })
        .then(async (account) => {
          console.log("ACCOUNT : ", account);
          stripe_bank_id = account.id;
          if (stripe_bank_id !== "") {
            let rez = await PaymentService.setStripeBankToken(
              stripe_bank_id,
              user_details[0]._id
            );
            res
              .status(200)
              .json({ status: 1, msg: "Added Bank Details", data: rez });
          }
        })
        .catch((error) => {
          console.log("bank account error", error.raw.message);
          res
            .status(200)
            .json({ status: 0, msg: error.raw.message, data: err });
        });
    });
  } catch (e) {
    console.log(e);
    return res.status(412).json({ status: 0, msg: e });
  }
};

exports.createCharge = async (req, res) => {
  try {
    // let data = req.body;
    // let stripe_customer_id = '';
    //create a token
    //  let = bank_token = '';
    // await stripe.tokens.create({
    //     bank_account: {
    //         country: 'US',
    //         currency: 'usd',
    //         account_holder_name: "sulay panchal",
    //         account_holder_type: 'individual',
    //         routing_number: '110000000',
    //         account_number: '000123456789'
    //     }
    // }).then(token => {
    //     bank_token = token.id
    // }).catch(err => console.log(err));
    // console.log("TOKEN : ", bank_token)
    // //create account
    // await stripe.accounts.create({
    //     type: 'custom',
    //     country: 'US',
    //     email: 'sulay.qriousetch@gmail.com',
    //     requested_capabilities: [
    //         'card_payments',
    //         'transfers'
    //     ],
    //     external_account: bank_token,
    //     business_profile: {
    //         mcc: 5045,
    //         url: 'https://cookies.com',
    //         name: 'Big Farm',
    //         product_description: 'Very big farm'
    //     },
    //     business_type: 'individual',
    //     tos_acceptance: {
    //         date: 1539356786,
    //         ip: '127.0.0.1'
    //     },
    //     individual: {
    //         id_number: '000000000',
    //         first_name: 'sulay',
    //         last_name: 'panchal',
    //         address: {
    //             city: 'Schenectady',
    //                 line1: '123 State St',
    //                 postal_code: 12345,
    //                 state: 'NY',
    //         },
    //         dob: {
    //             day: 10,
    //             month: 11,
    //             year: 1980
    //         },
    //         phone: 9913193001,
    //         email: 'sulay.qrioustech@gmail.com',
    //         // ssn_last_4: '1234'
    //     }
    // }).then((account) => {
    //     console.log("ACCOUNT : ", account);
    //     res.json({status:1,data:stripe_bank_id = account.id});
    // })
    // await stripe.account.update(
    //     'acct_1G9qIcBWNCXi0QkQ',
    //     {
    //         business_profile: {
    //             mcc: 5045,
    //             url: 'https://cookies.com',
    //             name: "Fern Farm",
    //             product_description: "We are growing Mango in our Farm"
    //         },
    //         business_type: 'individual',
    //         tos_acceptance: {
    //             date: 1539356786,
    //             ip: '127.0.0.1'
    //         },
    //         individual: {
    //             id_number: '000000000',
    //             first_name: 'Jaina',
    //             last_name: 'Trivedi',
    //             address: {
    //                 city: 'Schenectady',
    //                 line1: '123 State St',
    //                 postal_code: 12345,
    //                 state: 'NY',
    //             },
    //             dob: {
    //                 day: 10,
    //                 month: 11,
    //                 year: 1980
    //             },
    //             phone: 2015550123,
    //             email: "jenny@cookies.com",
    //             // ssn_last_4: '1234'
    //         },
    //         //
    //     }
    // ).then((account) => {
    //     console.log("ACCOUNT ", account)
    //     res.send(account)
    // });
    // Retrive account
    // await stripe.account.retrieve(
    //     'acct_1GAWs1LGubyx5nw8'
    // ).then((account) => {
    //     console.log("ACCOUNT ", account)
    //     res.send(account)
    // });
    // await stripe.charges.create(
    //     {
    //         amount : 1000,
    //         currency : 'usd',
    //         customer: 'cus_GhBJpvqUNZP5wU',
    //         application_fee_amount : 200,
    //         transfer_data : {
    //             destination: 'acct_1GAWs1LGubyx5nw8'
    //         }
    //     }
    // ).then(paumentIntent => {
    //     console.log(paumentIntent)
    //     res.send(paumentIntent)
    // })
    // if (stripe_customer_id !== '') {
    //     let rez = await PaymentService.setStripeToken(stripe_customer_id, data.user_id);
    //     res.status(200).json({ status: 1, msg: "Added Payment Info", data: rez });
    // }
  } catch (e) {
    console.log(e);
    return res.status(412).json({ status: 0, msg: e });
  }
};

//createPerson
// await stripe.account.updatePerson(
//     'acct_1G9nnALjkWGhCgs8',
//     'person_GhEMkO8Zso1QPD',
//     {
//         first_name : 'Jaina',
//         last_name : 'Trivedi',
//         relationship : {
//             executive : false,
//             representative : true,
//             title : 'CEO'
//         },
//         address : {
//             city : 'Schenectady',
//             line1 : '123 State St',
//             postal_code : 12345,
//             state : 'NY',
//         },
//         dob : {
//             day : 10,
//             month : 11,
//             year : 1980
//         },
//         id_number : 000000000,
//         phone : 2015550123,
//         email : "jenny@cookies.com"
//     }
// ).then((account) => {
//     console.log("ACCOUNT ", account)
//     res.send(account)
// });
//5b5ead
exports.demo_charge = async (req, res) => {
  try {
    await stripe.charges
      .create({
        amount: 1000,
        currency: "usd",
        customer: "cus_GgmwrRlGGQelPx",
        application_fee_amount: 140,
        capture: false,
        transfer_data: {
          destination: "acct_1GDm0NGqgpoSISEK",
        },
      })
      .then((paumentIntent) => {
        console.log("payment log", paumentIntent);
        res.status(200).json({ data: paumentIntent });
        // res.send(paumentIntent)
      });
  } catch (e) {
    console.log(e);
    res.status(412).json({ status: 0, msg: e });
  }
};

exports.demo_capture = async (req, res) => {
  try {
    stripe.charges.capture("ch_1GDmwwINeK2QJ6slevJAnOwi", function (
      err,
      charge
    ) {
      console.log(charge);
      res.status(200).json({ data: charge });
      // asynchronously called
    });
  } catch (e) {
    console.log(e);
    res.status(412).json({ status: 0, msg: e });
  }
};

exports.card_detail = async (req, res) => {
  try {
    let users = await User.findById(req.body.user_id);
    let card_detail = await stripe.customers.retrieve(users.stripe_customer_id);
    res.status(200).json({
      status: 1,
      //data: card_detail,
      data: {
        card_number: card_detail.sources.data[0].last4,
        exp_month: card_detail.sources.data[0].exp_month,
        exp_year: card_detail.sources.data[0].exp_year,
      },
      msg: "Card Detail Retrieve Successfully.",
    });
  } catch (e) {
    console.log(e);
    res.status(412).json({ status: 0, msg: e });
  }
};
exports.bank_detail = async (req, res) => {
  try {
    let users = await User.findById(req.body.user_id);
    let account_detail = await stripe.accounts.retrieve(users.stripe_bank_id);
    // console.log("ACCOUTN DETAIL", account_detail)
    res.status(200).json({
      status: 1,
      data: {
        bank_name: account_detail.external_accounts.data[0].bank_name,
        account_number: account_detail.external_accounts.data[0].last4,
        routing_number: account_detail.external_accounts.data[0].routing_number,
        dob: account_detail.individual.dob,
        account_holder_name:
          account_detail.individual.first_name +
          " " +
          account_detail.individual.last_name,
      },
      msg: "Bank Detail Retrieve Successfully.",
    });
  } catch (e) {
    console.log(e);
    res.status(412).json({ status: 0, msg: e });
  }
};

exports.create_token = async (req, res) => {
  try {
    stripe.tokens.create(
      {
        card: {
          number: "4111111111111111",
          exp_month: 4,
          exp_year: 2021,
          cvc: "314",
        },
      },
      function (err, token) {
        // asynchronously called
        res.status(200).json({ data: token });
      }
    );
  } catch (e) {
    console.log(e);
    res.status(412).json({ status: 0, msg: e });
  }
};

exports.save_card = async (req, res) => {
  try {
    await stripe.customers
      .create({
        name: "arrow",
        email: req.body.email,
        source: req.body.token,
      })
      .then((customer) => {
        res.status(200).json({ data: customer });
      });
  } catch (e) {
    console.log(e);
    res.status(412).json({ status: 0, msg: e });
  }
};
exports.add_new_card = async (req, res) => {
  try {
    let user_details = await UserService._getUserByUserId(req.body._id);
    console.log("user_details", user_details[0].stripe_customer_id);
    await stripe.customers.createSource(
      user_details[0].stripe_customer_id,
      { source: req.body.token },
      function (err, card) {
        res.status(200).json({ status: 1, msg: "Card Added Succcessfully." });
        // asynchronously called
      }
    );
  } catch (e) {
    console.log(e);
    res.status(412).json({ status: 0, msg: e });
  }
};

exports.retrive_customer = async (req, res) => {
  try {
    let user_details = await UserService._getUserByUserId(req.body._id);
    await stripe.customers.retrieve(
      user_details[0].stripe_customer_id,
      function (err, customer) {
        console.log("count", customer.sources.data.length);
        if (customer.sources.data.length > 0) {
          res.status(200).json({
            status: 1,
            data: customer.sources.data,
            default_card_id: customer.default_source,
          });
        } else {
          res.status(200).json({ status: 0, msg: "No Card Found." });
        }
        // asynchronously called
      }
    );
  } catch (e) {
    console.log(e);
    res.status(412).json({ status: 0, msg: e });
  }
};
exports.change_default_card = async (req, res) => {
  try {
    let user_details = await UserService._getUserByUserId(req.body._id);
    stripe.customers.update(
      user_details[0].stripe_customer_id,
      { default_source: req.body.card_id },
      function (err, customer) {
        if (err) {
          res.status(200).json({ status: 0, msg: err });
        } else {
          res
            .status(200)
            .json({ status: 1, msg: "Default Card Change Successfully." });
        }
      }
    );
  } catch (e) {
    console.log(e);
    res.status(412).json({ status: 0, msg: e });
  }
};
