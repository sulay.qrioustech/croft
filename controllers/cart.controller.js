const CartService = require("../services/cart.service");

exports.addtocart = async (req, res) => {
  try {
    let request = req.body;

    if (request.products.length <= 0) {
      return res
        .status(412)
        .json({ status: 0, msg: "Cart should have atleast one item" });
    }

    let rez = await CartService.addtocart(request);
    return res
      .status(200)
      .json({ status: 1, msg: "Product Added to cart", data: rez });
  } catch (e) {
    return res.status(412).json({ status: 0, msg: e });
  }
};

exports.cartlist = async (req, res) => {
  try {
    let user_id = req.body.user_id;
    let rez = await CartService.cartlist(user_id);

    if (rez === null) {
      return res
        .status(200)
        .json({ status: 1, msg: "CArt list is Empty", data: null });
    }
    let data;
    data = rez.toJSON();
    let farm_name = rez.owner_id.farm.name;
    delete data.owner_id; //= undefined;
    data.farm_name = farm_name;
    data.farm_details = rez.owner_id.farm;
    data.address_list = rez.user_id.address_list;

    return res
      .status(200)
      .json({ status: 1, msg: "cart List of User", data: data });
  } catch (e) {
    return res.status(412).json({ status: 0, msg: e });
  }
};

exports.deletecart = async (req, res) => {
  try {
    let user_id = req.body.user_id;
    let cart_id = req.body.cart_id;
    let rez = await CartService.deleteCart({
      user_id: user_id,
      cart_id: cart_id
    });
    return res.status(200).json({ status: 1, msg: "Cart deleted", data: rez });
  } catch (e) {
    return res.status(412).json({ status: 0, msg: e });
  }
};

exports.deletecartItem = async (req, res) => {
  try {
    let user_id = req.body.user_id;
    let product_id = req.body.product_id;
    let cart_id = req.body.cart_id;
    let rez = await CartService.deletecartItem({
      user_id: user_id,
      cart_id: cart_id,
      product_id: product_id
    });
    return res
      .status(200)
      .json({ status: 1, msg: "Cart Item deleted", data: rez });
  } catch (e) {
    return res.status(412).json({ status: 0, msg: e });
  }
};
