const mongoose = require("mongoose");
const User = mongoose.model("User");
const Product = mongoose.model("Product");
const ReportService = require("../services/report.service");
const common_function = require("../common_function/index");

exports.addreport = async (req, res) => {
    try {
        let report = req.body;
        let rez = await ReportService._addReport(report);
        console.log(rez);

        let user_details = await User.findOne({ _id: report.user_id });
        let owner_details;
        if (report.report_to == 'farm') {
            console.log("HERE")
            owner_details = await User.findOne({ _id: report.farm_id });
        } else {
            let product_details = await Product.findOne({ _id: report.product_id }).populate("user_id")
            owner_details = product_details.user_id
        }

        var usermessage =
            "Thank you for Reporting Issue"
        common_function.sendMail(
            user_details.email,
            "Your reported problem has been received. Thanks for contacting us.",
            usermessage
        );


        var ownermessage =
            "Somebody had Reported against your farm"
        common_function.sendMail(
            owner_details.email,
            req.body.message,
            ownermessage
        );

        var adminmessage =
            "User had reported to some farm"
        common_function.sendMail(
            "jaina.qrioustech@gmail.com",
            req.body.message,
            adminmessage
            
        );


        return res
            .status(200)
            .json({ status: 1, msg: "Report Added Sucessfully", data: rez });
    } catch (e) {
        console.log("error",e)
        return res.status(412).json({ status: 0, msg: e });
    }
};