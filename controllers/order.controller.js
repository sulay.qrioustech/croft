const OrderService = require("../services/order.service");
const ProductService = require("../services/product.service");
const CartService = require("../services/cart.service");
const PaymentService = require("../services/payment.service");
const Cart = require("../models/cart.model");

exports.placeorder = async (req, res) => {
  try {
    let order = req.body;
    let check_quantity = await OrderService.check_quantity(order);
    if (check_quantity == 0) {
      let data = await OrderService.placeorder(order);
      console.log("DATA => ",data);
      if(!data){
        return res.status(200).json({
          status: 0,
          msg: "Delivery not available at this location"
        });
      }

      let product_details = await OrderService.getProductDetail(order);
      OrderService.reduce_quantity(order);
      return res.status(200).json({
        status: 1,
        msg: "Order Placed Sucessfully",
        data: product_details
      });
    } else {
      return res
        .status(200)
        .json({ status: 0, msg: "Prodcut Quantity is not available" });
    }
    //console.log('product details',product_details.products[0]);
    // let total = 0;
    // let user_id = '';
    // product_details = product_details.toJSON()
    // product_details.products.map((value)=>{
    //     //console.log('user_id',value.product_id.user_id);
    //     user_id = value.product_id.user_id
    //     total = total + parseFloat(value.product_id.price);
    //     return value.product_id.price
    // });
    // //console.log('user_id',user_id);
    // // console.log('total',total);
    // // console.log('user_id',user_id);
    // order.total = total
    // order.soldby_id = user_id

    //let rez = await OrderService.placeorder(order);
  } catch (e) {
    return res.status(412).json({ status: 0, msg: e });
  }
};

exports.acceptorder = async (req, res) => {
  try {
    let order = req.body;
    const response = await OrderService.acceptorder(order);
    return res
      .status(200)
      .json({ status: 1, msg: "Order Accepted Successfully", data: response });
  } catch (e) {
    return res.status(412).json({ status: 0, msg: e });
  }
};

exports.rejectorder = async (req, res) => {
  try {
    let order = req.body;
    const response = await OrderService.rejectorder(order);
    console.log("res", response);
    if (response) {
      return res.status(200).json({
        status: 1,
        msg: "Order Rejected Successfully",
        data: response
      });
    } else {
      return res.status(412).json({ status: 0, msg: "Something went wrong." });
    }
  } catch (e) {
    return res.status(412).json({ status: 0, msg: e });
  }
};

exports.rescheduleorder =  async (req, res) => {
  try {
    let order = req.body;
    const response = await OrderService.rescheduleorder(order);
    console.log("res", response);
    if (response) {
      return res.status(200).json({
        status: 1,
        msg: "Order Rescheduled Successfully",
        data: response
      });
    } else {
      return res.status(412).json({ status: 0, msg: "Something went wrong." });
    }
  } catch (e) {
    return res.status(412).json({ status: 0, msg: e });
  }
};

exports.trackorder = async (req, res) => {
  try {
    let order = req.body;
    const response = await OrderService.trackorder(order);
    console.log(response)
    return res
      .status(200)
      .json({ status: 1, msg: "Start Order Tracking", data: response });
  } catch (e) {
    console.log("ERROR",e)
    return res.status(412).json({ status: 0, msg: e });
  }
};

exports.listorders = async (req, res) => {
  res.send("List Order APi");
};

exports.boughtorder = async (req, res) => {
  try {
    let data = req.body;
    const response = await OrderService.boughtorder(data);
    if (response.length > 0) {
      let datajson;
      datajson = JSON.parse(JSON.stringify(response));
      datajson.forEach(item => {
        item.farm_id = item.owner_id._id
        item.farm_name = item.owner_id.farm.name;
        item.farm_address = item.owner_id.farm.pickup;

        if (item.order_type == "pickup") {
          item.delivery_charge = 0;
        } else {
          item.delivery_charge = item.delivery_charge;
          item.address = item.address;
        }
        delete item.owner_id;
      });
      return res
        .status(200)
        .json({ status: 1, msg: "Order Bought List", data: datajson });
    } else {
      return res.status(200).json({ status: 0, msg: "No Orders Found." });
    }
  } catch (e) {
    return res.status(412).json({ status: 0, msg: e });
  }
};

exports.sellorder = async (req, res) => {
  try {
    let data = req.body;
    let response = await OrderService.sellorder(data);
    console.log("response", response.length);
    if (response.length > 0) {
      console.log("DAta => ");
      let datajson;
      datajson = JSON.parse(JSON.stringify(response));
      datajson.forEach(item => {
        item.farm_id = item.owner_id._id
        item.userid = item.user_id._id
        item.farm_name = item.owner_id.farm.name;
        item.farm_address = item.owner_id.farm.pickup;
        item.user_name = item.user_id.fname + " " + item.user_id.lname;
        if (item.order_type == "pickup") {
          if (item.user_id.isFarmUpdated) {
            item.address = item.user_id.farm.pickup.address;
          } else {
            item.address = "";
          }
          item.delivery_charge = 0;
        } else {
          item.delivery_charge = item.delivery_charge;
          item.address = item.address;
        }
        delete item.owner_id;
        delete item.user_id;
      });

      return res
        .status(200)
        .json({ status: 1, msg: "Order Sell List", data: datajson });
    } else {
      return res.status(200).json({ status: 0, msg: "No Orders Found." });
    }
  } catch (e) {
    console.log(e);
    return res.status(412).json({ status: 0, msg: e });
  }
};

exports.confirmorder = async (req, res) => {
  try {
    let card_data = await Cart.findById(req.body.cart_id);
    console.log("CAPTURE_ID => ",card_data.capture_id);
    let payment = await PaymentService.capture_transaction(
      card_data.capture_id
    );
    console.log("payment =>", payment);
    if (payment ==  true) {
      let response = await OrderService.confirmorder(req.body.cart_id);
      return res.status(200).json({
        status: 1,
        msg: "Order Completed Successfully.",
        data: response
      });
    } else {
      return res.status(412).json({
        status: 0,
        msg: "Something went wrong please try again.",
        error : payment 
      });
    }
  } catch (e) {
    console.log(e);
    return res.status(412).json({ status: 0, msg: e });
  }
};

exports.getBookedDeliverySlots = async (req,res) => {
  try {
    let cart_data = await Cart.find({ owner_id : req.body.farm_id, date : req.body.date },{ "time_slot" : 1 });
    
    if(cart_data.length == 0){
      res.status(200).json({ status: 0, msg: "No Timeslots Booked" })
    }else{

      cart_data = cart_data.map(item => item.time_slot)

      res.status(200).json({ status: 1, msg: "Booked TimeSlots", data: cart_data  })
    }

  }catch(e){

  }
}
