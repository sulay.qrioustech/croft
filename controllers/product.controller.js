const ProductService = require("../services/product.service");
var moment = require("moment");
exports.add = async (req, res) => {
  try {
    let product = req.body;
    let rez = await ProductService._addProduct(product);
    return res
      .status(200)
      .json({ status: 1, msg: "Product Added Sucessfully", data: rez });
  } catch (e) {
    return res.status(412).json({ status: 0, msg: e });
  }
};

exports.update = async (req, res) => {
  try {
    let request = req.body;
    request._id = req.body.product_id;
    let rez = await ProductService._updateProducts(request);
    if (!rez) {
      return res.status(412).json({ status: 0, msg: "Please try again later" });
    }
    return res
      .status(200)
      .json({ status: 1, msg: "Product updated", data: rez });
  } catch (e) {
    return res.status(412).json({ status: 0, msg: e });
  }
};

exports.delete = async (req, res) => {
  try {
    let request = req.body;
    request._id = req.body.product_id;
    let rez = await ProductService._deleteProducts(request);
    if (!rez) {
      return res.status(412).json({ status: 0, msg: "Please try again later" });
    }
    return res
      .status(200)
      .json({ status: 1, msg: "Product deleted", data: rez });
  } catch (e) {
    return res.status(412).json({ status: 0, msg: e });
  }
};

exports.listByUserid = async (req, res) => {
  try {
    let user_id = req.body.user_id;
    let rez = await ProductService._getProductsByUserId(user_id);
    return res
      .status(200)
      .json({ status: 1, msg: "Product List of User", data: rez });
  } catch (e) {
    return res.status(412).json({ status: 0, msg: e });
  }
};

exports.filterlistByUserid = async (req, res) => {
  try {
    let user_id = req.body.user_id;
    let rez = await ProductService._getFilterProductsByUserId(user_id);
    let upcomingList = [];
    let currentList = [];
    rez.map(record => {
      let harvesting_date = record.harvesting_date;
      let d = moment(harvesting_date).isBefore(moment());
      if (d) {
        currentList.push(record);
      } else {
        upcomingList.push(record);
      }
    });

    let response = {
      upcomingList: upcomingList,
      currentList: currentList
    };

    return res
      .status(200)
      .json({ status: 1, msg: "Product List of User", data: response });
  } catch (e) {
    return res.status(412).json({ status: 0, msg: e });
  }
};

exports.detailByProductId = async (req, res) => {
  try {
    //let user_id = req.body.user_id;
    let product_id = req.body.product_id;
    let rez = await ProductService._getProductByProductId(product_id);
    return res
      .status(200)
      .json({ status: 1, msg: "Product Detail", data: rez });
  } catch (e) {
    return res.status(412).json({ status: 0, msg: e });
  }
};

//Admin panel

exports.list = async (req, res) => {
  try {
    let rez = await ProductService._getProducts();
    return res
      .status(200)
      .json({ status: 1, msg: "All Product List", data: rez });
  } catch (e) {
    return res.status(412).json({ status: 0, msg: e });
  }
};
