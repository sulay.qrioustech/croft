const FollowService = require('../services/follow.service')


exports.change_follow_status = async (req, res) => {
    try {
        let request = req.body;

        let rez = await FollowService.get_follow_status(request);
        let response;
        if(rez){
            response = await FollowService.remove_status(request);
        }else{
            response = await FollowService.add_status(request);
        }
        return res.status(200).json({ status: 1, msg: "Following Status changed", data: response });
    } catch (e) {
        return res.status(412).json({ status: 0, msg: e });
    }
}

exports.get_follow_status = async (req, res) => {
    try {
        let request = req.body;
        let rez = await FollowService.get_follow_status(request);
        return res.status(200).json({ status: 1, msg: "Succesfully get follow status of user", data: rez });
    } catch (e) {
        return res.status(412).json({ status: 0, msg: e });
    }
}