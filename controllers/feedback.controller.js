const FeedbackService = require('../services/feedback.service')


exports.addUserFeedback = async (req, res) => {
    try {
        let request = req.body;
        // request._id = req.body.user_id
        // delete request.user_id
        let rez = await FeedbackService._addFeedback(request);
        return res.status(200).json({ status: 1, msg: "Feedback updated", data: rez });
    } catch (e) {
        return res.status(412).json({ status: 0, msg: e });
    }
}