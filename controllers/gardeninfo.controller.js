const GardeninfoService = require('../services/gardeninfo.service')

exports.getGardenInfo = async (req, res) => {
    try {
        let rez = await GardeninfoService._getGardenInfo();
        return res.status(200).json({ status: 1, msg: "GardenInfo", data: rez });
    } catch (e) {
        return res.status(412).json({ status: 0, msg: e });
    }
}