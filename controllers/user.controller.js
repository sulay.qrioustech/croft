const UserService = require("../services/user.service");
const ReviewService = require("../services/review.service");
const stripe = require("../services/stripe.service");
const common_function = require("../common_function/index");
const mongoose = require("mongoose");
const About = mongoose.model("About");
const User = mongoose.model("User");

exports.updateUsers = async (req, res) => {
  try {
    let request = req.body;
    request._id = req.body.user_id;
    delete request.user_id;
    if (request.email) {
      delete request.email;
    }

    if (request.password) {
      delete request.password;
    }

    if (request.phone) {
      delete request.phone;
    }

    if (request.email_verification_code) {
      delete request.email_verification_code;
    }

    if (request.email_verification_status) {
      delete request.email_verification_status;
    }

    if (request.userType) {
      delete request.userType;
    }

    //updating User adress

    let add = [];
    let obj = {};
    if (request.address) {
      obj = {
        ...obj,
        address: request.address
      };
      delete request.address;
      add = [obj];
    }

    if (request.city) {
      obj = {
        ...obj,
        city: request.city
      };
      delete request.city;
      add = [obj];
    }

    if (request.state) {
      obj = {
        ...obj,
        state: request.state
      };
      delete request.state;
      add = [obj];
    }

    if (request.zipcode) {
      obj = {
        ...obj,
        zipcode: request.zipcode
      };
      delete request.zipcode;
      add = [obj];
    }

    if (request.lat) {
      obj = {
        ...obj,
        lat: request.lat
      };
      delete request.lat;
      add = [obj];
    }

    if (request.long) {
      obj = {
        ...obj,
        long: request.long
      };
      delete request.long;
      add = [obj];
    }

    if (add.length > 0) {
      request.address_list = add;
    }

    request.isFarmUpdated = true;
    if (!request.farm) {
      request.farm = {};
      request.location = {
        type: "Point",
        coordinates: [0, 0]
      };
      request.isFarmUpdated = false;
    } else {
      if (!request.farm.name) request.isFarmUpdated = false;
      if (!request.farm.about_farm) request.isFarmUpdated = false;
      if (!request.farm.pickup) {
        request.isFarmUpdated = false;
        request.location = {
          type: "Point",
          coordinates: [0, 0]
        };
      } else {
        if (!request.farm.pickup.address) request.isFarmUpdated = false;
        if (!request.farm.pickup.state) request.isFarmUpdated = false;
        if (!request.farm.pickup.zipcode) request.isFarmUpdated = false;
        if (!request.farm.pickup.city) request.isFarmUpdated = false;
        if (!request.farm.pickup.lat) request.isFarmUpdated = false;
        if (!request.farm.pickup.long) request.isFarmUpdated = false;
        if (request.farm.pickup.lat && request.farm.pickup.long) {
          request.location = {
            type: "Point",
            coordinates: [request.farm.pickup.long, request.farm.pickup.lat]
          };
        } else {
          request.location = {
            type: "Point",
            coordinates: [0, 0]
          };
        }
      }

      if (!request.farm.time) request.isFarmUpdated = false;
      if (!request.farm.delivery_range) request.isFarmUpdated = false;
      if (!request.farm.delivery_charge) request.isFarmUpdated = false;
      // if (!request.farm.gardeninfo) request.isFarmUpdated = false;
      if (!request.farm.days) request.isFarmUpdated = false;
      if (!request.farm.deliveryType) request.isFarmUpdated = false;
    }
    //return res.status(200).json({ status: 1, msg: "User updated", data: request });
    let rez = await UserService._updateUser(request);
    console.log("rez", rez);
    if (!rez) {
      return res.status(412).json({ status: 0, msg: "Please try again later" });
    }
    if (rez.password) {
      rez.password = undefined;
    }

    if (rez.email_verification_code) {
      rez.email_verification_code = undefined;
    }
    return res.status(200).json({ status: 1, msg: "User updated", data: rez });
  } catch (e) {
    console.log(e);
    return res.status(412).json({ status: 0, msg: e });
  }
};

exports.userDetail = async (req, res) => {
  try {
    let user_id = req.body.user_id;
    let rez = await UserService._getUserByUserId(user_id);
    if (!rez) {
      return res.status(412).json({ status: 0, msg: "Please try again later" });
    }
    if (rez[0].password) {
      rez[0].password = undefined;
    }

    if (rez[0].email_verification_code) {
      rez[0].email_verification_code = undefined;
    }
    return res
      .status(200)
      .json({ status: 1, msg: "User Detail", data: rez[0] });
  } catch (e) {
    return res.status(412).json({ status: 0, msg: e });
  }
};

exports.userfarmdetail = async (req, res) => {
  try {
    let user_id = req.body.user_id;
    let rez = await UserService._getFarmByUserId(user_id);
    return res
      .status(200)
      .json({ status: 1, msg: "User farm Detail", data: rez });
  } catch (e) {
    return res.status(412).json({ status: 0, msg: e });
  }
};

exports._getNearbyFarms = async (req, res) => {
  try {
    let data = {
      latitude: req.body.latitude,
      longitude: req.body.longitude,
      user_id : req.body.user_id
    };
    let rez = await UserService._getNearbyFarms(data);
    let farms_list = rez;
    farm_list = farms_list.map((farm, index) => {
      delete farm.password;
      delete farm.email_verification_code;
      delete farm.email_verification_status;
      delete farm.userType;
      delete farm.phone;
      delete farm.address_list;
      delete farm.location;
      farm.follow_id && delete farm.follow_id
      let products_name = farm.product.map(item => item.product_name);
      let products_images = farm.product.map(item => {
        let image = item.image;
        if (image.length > 0) {
          return image[0];
        } else {
          return undefined;
        }
      });
      if (products_name.length > 2) {
        products_name = products_name.slice(0, 3);
      }

      //console.log(farm)

      let total_ratings = 0;
      farm.review.map(rate => {
        total_ratings = total_ratings + parseFloat(rate.rating);
        return rate;
      });

      // farm.review = farm.review.map(item => {

      //     return {
      //         name: item.from_id.fname + " " + item.from_id.lname,
      //         from_id: item.from_id._id,
      //         given_id: item.given_id,
      //         text: item.text
      //     }
      // })
      let avg_rating = 0;
      if (farm.review.length > 0) {
        avg_rating = total_ratings / farm.review.length;
      }
      farm.avg_rating = avg_rating;
      farm.products_images = products_images;
      farm.products_name = products_name.join();
      delete farm.product;
      return farm;
    });

    return res
      .status(200)
      .json({ status: 1, msg: "User farm List", data: farm_list });
  } catch (e) {
    return res.status(412).json({ status: 0, msg: e });
  }
};

exports._getNearbyFarmsFilter = async (req, res) => {
  try {
    let data = {
      latitude: req.body.latitude,
      longitude: req.body.longitude,
      delivery_type: req.body.delivery_type,
      delivery_distance: req.body.delivery_distance,
      organically_grown: req.body.organically_grown
    };
    let rez = await UserService._getNearbyFarmsFilter(data);
    let farms_list = rez;

    if (req.body.organically_grown === "true") {
      console.log("TRUE");
      farms_list = farms_list.map(farm => {
        farm.product = farm.product.map(item => {
          if (item.organically_grown) {
            return item;
          } else {
            return undefined;
          }
        });
        farm.product = farm.product.filter(f => f !== undefined);

        if (farm.product.length > 0) {
          return farm;
        } else {
          return undefined;
        }
      });
    } else if (req.body.organically_grown === "false") {
      console.log("FALSe");

      farms_list = farms_list.map(farm => {
        farm.product = farm.product.map(item => {
          if (!item.organically_grown) {
            return item;
          } else {
            return undefined;
          }
        });
        farm.product = farm.product.filter(f => f !== undefined);
        if (farm.product.length > 0) {
          return farm;
        } else {
          return undefined;
        }
      });
    }
    //        console.log(farms_list)

    farms_list = farms_list.filter(f => f !== undefined);

    farm_list = farms_list.map((farm, index) => {
      delete farm.password;
      delete farm.email_verification_code;
      delete farm.email_verification_status;
      delete farm.userType;
      delete farm.phone;
      delete farm.address_list;
      delete farm.location;

      let products_name = farm.product.map(item => item.product_name);
      let products_images = farm.product.map(item => {
        let image = item.image;
        if (image.length > 0) {
          return image[0];
        } else {
          return undefined;
        }
      });

      if (products_name.length > 2) {
        products_name = products_name.slice(0, 3);
      }
      let total_ratings = 0;
      farm.review.map(rate => {
        total_ratings = total_ratings + parseFloat(rate.rating);
        return rate;
      });

      let avg_rating = 0;
      if (farm.review.length > 0) {
        avg_rating = total_ratings / farm.review.length;
      }
      farm.avg_rating = avg_rating;
      farm.products_images = products_images;
      farm.products_name = products_name.join();
      delete farm.product;
      return farm;
    });
    return res
      .status(200)
      .json({ status: 1, msg: "User farm List", data: farms_list });
  } catch (e) {
    return res.status(412).json({ status: 0, msg: e });
  }
};

// exports._getFarms = async (req, res) => {
//     try {
//         let rez = await UserService._getFarms();
//         return res.status(200).json({ status: 1, msg: "User farms List", data: rez });
//     } catch (e) {
//         return res.status(412).json({ status: 0, msg: e });
//     }
// }

//Admin Panel

exports.signup = async (req, res) => {
  try {
    let rez = await UserService._signup(req);
    return res.status(200).json(rez);
  } catch (e) {
    return res.status(412).json({ status: 0, msg: e });
  }
};

exports.signin = async (req, res) => {
  try {
    let rez = await UserService._signin({
      email: req.body.email,
      password: req.body.password
    });
    return res.status(200).json(rez);
  } catch (e) {
    return res.status(412).json({ status: 0, msg: e });
  }
};

exports.userslist = async (req, res) => {
  try {
    let _id = req.body.admin_id;
    let rez = await UserService._getUsersForAdmin(_id);
    return res
      .status(200)
      .json({ status: 1, msg: "All Users List", data: rez });
  } catch (e) {
    return res.status(412).json({ status: 0, msg: e });
  }
};

exports.farmslist = async (req, res) => {
  try {
    let rez = await UserService._getFarms();
    return res.status(200).json({ status: 1, msg: "Farm List", data: rez });
  } catch (e) {
    console.log(e);
    return res.status(412).json({ status: 0, msg: e });
  }
};

exports.list = async (req, res) => {
  try {
    // let _id = res.body.user_id
    let rez = await UserService._getUsers();
    return res
      .status(200)
      .json({ status: 1, msg: "All Users List", data: rez });
  } catch (e) {
    return res.status(412).json({ status: 0, msg: e });
  }
};

exports.add_address = async (req, res) => {
  try {
    let rez = await UserService.add_address(req);
    return res
      .status(200)
      .json({ status: 1, msg: "Address Added Successfully.", data: rez });
  } catch (e) {
    return res.status(412).json({ status: 0, msg: e });
  }
};

exports.orderDetail = async (req, res) => {
  try {
    let rez = await UserService.order_list();
    return res.status(200).json({ status: 1, msg: "Orders List", data: rez });
  } catch (e) {
    return res.status(412).json({ status: 0, msg: e });
  }
};

exports.productDetail = async (req, res) => {
  try {
    data = req.body;
    let rez = await UserService.product_details(data);
    return res
      .status(200)
      .json({ status: 1, msg: "Product Details", data: rez });
  } catch (e) {
    return res.status(412).json({ status: 0, msg: e });
  }
};

exports.order_details = async (req, res) => {
  try {
    data = req.body;
    let rez = await UserService.order_details(data);
    return res.status(200).json({ status: 1, msg: "Order Details", data: rez });
  } catch (e) {
    return res.status(412).json({ status: 0, msg: e });
  }
};

// exports.save_card = async (req,res) => {
//     try {
//         let data = req.body;
//         let stripe_acc_id='';

//         await stripe.customers.createSource(
//             'cus_GfmKr364jiRYKq',
//             {
//                 source: {
//                     "country": "US",
//                     "currency": "usd",
//                     "account_holder_name": "Jenny Rosen",
//                     "account_holder_type": "individual",
//                     "account_number" : ""
//                 }
//             },
//             (err, bankAccount) => {
//                 // asynchronously called
//                 stripe_acc_id = bankAccount.id;
//             }
//         );

//         if (stripe_acc_id!==''){
//             let rez = await UserService.setStripeToken(stripe_customer_id, data.user_id);
//             res.status(200).json({ status: 1, msg: "Added Payment Info", data: rez });
//         }

//     }catch(e){
//         return res.status(412).json({ status: 0, msg: e })
//     }
// }

// exports.savetoken = async (req, res) => {
//     try {
//         let data = req.body;
//         let stripe_customer_id = '';

//         await stripe.customers
//             .create({
//                 name: data.token,
//                 email: data.email,
//                 source: data.token
//             })
//             .then(customer => {
//                 stripe_customer_id = customer.id;
//             }).catch(err => console.log(err));
//         if (stripe_customer_id !== '') {
//             let rez = await UserService.setStripeToken(stripe_customer_id, data.user_id);
//             res.status(200).json({ status: 1, msg: "Added Payment Info", data: rez });
//         }

//     } catch (e) {
//         return res.status(412).json({ status: 0, msg: e })
//     }
// }
exports.logout = async (req, res) => {
  try {
    let rez = await UserService.logout(req.body.user_id);
    res.status(200).json({ status: 1, msg: "Logout Successfully" });
  } catch (e) {
    return res.status(412).json({ status: 0, msg: e });
  }
};

exports.graph = async (req, res) => {
  try {
    let data = req.body;
    //let rez = await UserService.graph(data);
    //console.log('result',rez);
    const arr = [
      0,
      0,
      0,
      0,
      0,
      0,
      4,
      7,
      2,
      6,
      2,
      6,
      2,
      8,
      4,
      2,
      7,
      2,
      3,
      1,
      3,
      1,
      4,
      2,
      4,
      1,
      0,
      2,
      5,
      1
    ];
    console.log("length", arr.length);
    res.status(200).json({ status: 1, msg: "success", data: arr });
  } catch (e) {
    return res.status(412).json({ status: 0, msg: e });
  }
};

exports.send_notification = async (req, res) => {
  try {
    let data = req.body;
    let rez = await UserService.getUserFcmIds(data);
    //console.log('fcm_ids',rez);

    var payload = {
      notification: {
        title: data.title,
        body: data.description,
        noti_type: "0"
      }
    };
    await UserService.addNotification(data);
    common_function.send_admin_notification(payload, rez);
    res.status(200).json({ status: 1, msg: "Notification Sent Successfully." });
  } catch (e) {
    return res.status(412).json({ status: 0, msg: e });
  }
};

exports.get_notification = async (req, res) => {
  try {
    let rez = await UserService.get_notification();
    res.status(200).json({ status: 1, data: rez, msg: "Success." });
  } catch (e) {
    return res.status(412).json({ status: 0, msg: e });
  }
};

exports.get_aboutus = async (req, res) => {
  try {
    let response = await UserService.get_aboutus();
    res.status(200).json({ status: 1, data: response, msg: "Success" });
  } catch (e) {
    return res.status(412).json({ status: 0, msg: e });
  }
};

exports.update_about = async (req, res) => {
  try {
    let data = req.body;
    let response = await UserService.update_about(data);
    res.status(200).json({
      status: 1,
      data: response,
      msg: "About us updated successfully"
    });
  } catch (e) {
    return res.status(412).json({ status: 0, msg: e });
  }
};

exports.add_about = async (req, res) => {
  try {
    let about = new About();
    about.description = "this is lorem ipsum";
    about.save((err, result) => {
      if (!err) {
        console.log("result ", result);
        return about;
      } else {
        console.log("error", err);
        return { err };
      }
    });
  } catch (e) {
    return res.status(412).json({ status: 0, msg: e });
  }
};

exports.add_faq = async (req, res) => {
  try {
    let list = await UserService.add_faq(req.body);
    return res
      .status(200)
      .json({ status: 1, msg: "Faq added successfully.", data: list });
  } catch (e) {
    return res.status(412).json({ status: 0, msg: e });
  }
};

exports.edit_faq = async (req, res) => {
  try {
    let list = await UserService.edit_faq(req.body);
    return res
      .status(200)
      .json({ status: 1, msg: "Faq added successfully.", data: list });
  } catch (e) {
    return res.status(412).json({ status: 0, msg: e });
  }
};

exports.get_faq = async (req, res) => {
  try {
    let list = await UserService.get_faq(req.body);
    return res
      .status(200)
      .json({ status: 1, msg: "Faq get successfully.", data: list });
  } catch (e) {
    return res.status(412).json({ status: 0, msg: e });
  }
};

exports.faq = async (req, res) => {
  try {
    let list = await UserService.faq(req.body);
    return res
      .status(200)
      .json({ status: 1, msg: "Faq get successfully.", data: list });
  } catch (e) {
    return res.status(412).json({ status: 0, msg: e });
  }
};

exports.delete_faq = async (req, res) => {
  try {
    let list = await UserService.delete_faq(req.body);
    return res
      .status(200)
      .json({ status: 1, msg: "Faq remove successfully.", data: list });
  } catch (e) {
    return res.status(412).json({ status: 0, msg: e });
  }
};

exports.feedback = async (req, res) => {
  try {
    let list = await UserService.feedback();
    return res
      .status(200)
      .json({ status: 1, msg: "Feedback List", data: list });
  } catch (e) {
    return res.status(412).json({ status: 0, msg: e });
  }
};

exports.transaction_history = async (req, res) => {
  try {
    let list = await UserService.transaction_history();
    return res.status(200).json({ status: 1, msg: "success", data: list });
  } catch (e) {
    return res.status(412).json({ status: 0, msg: e });
  }
};

exports.verified_email = async (req, res) => {
  try {
    let check = await User.findOne({
      email: req.body.email,
      email_verification_code: req.body.code
    });
    console.log(check);
    if (check != null) {
      let response = await User.findOneAndUpdate(
        { _id: check._id },
        {
          email_verification_status: 1
        },
        { new: true, upsert: true }
      );
      res
        .status(200)
        .json({
          status: 1,
          msg: "Email Verified Successfully.",
          data: response
        });
    } else {
      res.status(200).json({
        status: 0,
        msg: "Invalid Verification code."
      });
    }
  } catch (e) {
    return res.status(412).json({ status: 0, msg: e });
  }
};
