const ReviewService = require('../services/review.service')

exports.add = async (req, res) => {
    try {
        let review = req.body;
        let rez = await ReviewService._addReview(review);
        return res.status(200).json({ status: 1, msg: "Review Added Sucessfully", data: rez });
    } catch (e) {
        return res.status(412).json({ status: 0, msg: e });
    }
}

exports.list = async (req, res) => {
    try {
        let rez = await ReviewService._getReviewsByUserId(req.body.user_id);
        rez = rez.map(item => { 

            return {
                name: item.from_id.fname + " " + item.from_id.lname, 
                from_id : item.from_id._id,
                given_id : item.given_id,
                text : item.text,
                rating : item.rating
            }
        })
        return res.status(200).json({ status: 1, msg: "All Review List", data: rez });
    } catch (e) {
        return res.status(412).json({ status: 0, msg: e });
    }
}

exports.canreview = async (req,res) => {
    try {
        //{ user_id : req.body.user_id, farm_id : req.body.farm_id }
        let rez = await ReviewService._canUserReview(req.body);
        return res.status(200).json({ status: 1, msg: "Can User Review this Farm", data: rez });
    } catch (e) {
        return res.status(412).json({ status: 0, msg: e });
    }
}